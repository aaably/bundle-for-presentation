.PHONY: help install test report unit-test unit-report functional-test functional-report

help:
	@echo "Available commands:"
	@echo ""
	@echo "  help               list of available make commands"
	@echo "  install            installs all dependencies (composer, npm, bower)"
	@echo "  test               launch all tests (unit, functional)"
	@echo "  report             generate code coverage reports for unit and functional tests"
	@echo "  unit-test          launch PHPUnit unit tests"
	@echo "  unit-report        launch PHPUnit unit tests with code coverage reports"
	@echo "  functional-test    launch PHPUnit functional tests"
	@echo "  functional-report  launch PHPUnit functional tests with code coverage reports"
	@echo ""

install:
	composer update

test: unit-test functional-test

report: unit-report functional-report

unit-test:
	@echo "PHPUNIT -- UNIT TESTS"
	vendor/bin/phpunit -c build/phpunit_unit_reports.xml -v --debug --no-coverage

unit-report:
	@echo "PHPUNIT -- UNIT TESTS -- WITH CODE COVERAGE REPORT"
	vendor/bin/phpunit -c build/phpunit_unit_reports.xml -v --debug

functional-test:
	@echo "PHPUNIT -- FUNCTIONAL TESTS"
	vendor/bin/phpunit -c build/phpunit_functional_reports.xml -v --debug --no-coverage

functional-report:
	@echo "PHPUNIT -- FUNCTIONAL TESTS -- WITH CODE COVERAGE REPORT"
	vendor/bin/phpunit -c build/phpunit_functional_reports.xml -v --debugs