<?php

namespace Fitatu\BillingBundle\Provider;

use Fitatu\DatabaseBundle\Entity\Auth\Payment;
use Illuminate\Support\Collection;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
interface BillingProviderInterface
{
    /**
     * @param int        $userId
     * @param Collection $transaction
     * @return bool
     */
    public static function validate(int $userId, Collection $transaction);

    /**
     * @param Payment $payment
     * @return bool
     */
    public static function refund(Payment $payment): bool;

    /**
     * @param Payment $payment
     * @return bool
     */
    public static function cancel(Payment $payment): bool;

    /**
     * @param Collection $transaction
     *
     * @return \DateTime
     */
    public static function getCompletedTimeFromPayload(Collection $transaction);
}
