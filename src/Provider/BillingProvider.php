<?php

namespace Fitatu\BillingBundle\Provider;

use Symetria\SharedBundle\Mapper\UserSystemMapper;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class BillingProvider
{
    public const PROVIDERS = UserSystemMapper::MAP;
    public const FALLBACK_PROVIDER = UserSystemMapper::WEB;

    public const ANDROID_PROVIDER_KEY = 'android-playstore';
    public const IOS_PROVIDER_KEY = 'ios-appstore';

    public const PROVIDER_MAPPER = [
        UserSystemMapper::ANDROID => AndroidBillingProvider::class,
        UserSystemMapper::IOS     => IosBillingProvider::class,
        UserSystemMapper::WEB     => WebBillingProvider::class,
        UserSystemMapper::WINDOWS => WebBillingProvider::class,
    ];

    public const PROVIDER_TYPE_LOOKUP_TABLE = [
        self::ANDROID_PROVIDER_KEY => UserSystemMapper::ANDROID,
        self::IOS_PROVIDER_KEY     => UserSystemMapper::IOS,
    ];

    /**
     * @param string $key
     * @return BillingProviderInterface
     */
    public static function get(string $key): BillingProviderInterface
    {
        /** @var BillingProviderInterface $name */
        $name = static::getName($key); // @example Android

        return new $name();
    }

    /**
     * @param string $key
     * @return string
     */
    public static function getName(string $key): string
    {
        if (!array_key_exists($key, static::PROVIDER_MAPPER)) {
            $key = static::FALLBACK_PROVIDER;
        }

        return static::PROVIDER_MAPPER[$key];
    }

    /**
     * @param string $name
     * @return int
     */
    public static function getId(string $name): int
    {
        if (!array_key_exists($name, static::PROVIDERS)) {
            $name = static::FALLBACK_PROVIDER;
        }

        return static::PROVIDERS[$name];
    }

    /**
     * @param string $type
     * @return string|null
     */
    public static function translateTypeToSystem(string $type): ?string
    {
        if (!array_key_exists($type, static::PROVIDER_TYPE_LOOKUP_TABLE)) {
            return null;
        }

        return static::PROVIDER_TYPE_LOOKUP_TABLE[$type];
    }

    /**
     * @param string $type
     * @return BillingProviderInterface
     */
    public static function getByTransactionType(string $type): BillingProviderInterface
    {
        return static::get(
            static::translateTypeToSystem($type)
        );
    }
}
