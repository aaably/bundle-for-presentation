<?php

namespace Fitatu\BillingBundle\Provider;

use Fitatu\BillingBundle\Exception\EmptyAndroidPurchaseTokenException;
use Fitatu\DatabaseBundle\Entity\Auth\Payment;
use Illuminate\Support\Collection;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class AndroidBillingProvider implements BillingProviderInterface
{
    const APP_NAME = 'Fitatu';
    const KEY_PATH = '/var/www/klucze/google_oauth.json';

    const APP_PACKAGE_NAME = 'com.fitatu.tracker';

    const PAYMENT_PENDING = 0;
    const PAYMENT_RECEIVED = 1;
    const PAYMENT_FREE_TRIAL = 2;

    public const CANCELLED_BY_USER = 0;
    public const CANCELLED_BY_SYSTEM = 1;
    public const CANCELLED_BY_SUBSCRIPTION = 2;

    private const CANCEL_REASONS = [
        self::CANCELLED_BY_USER         => 'User cancelled the subscription',
        self::CANCELLED_BY_SYSTEM       => 'Subscription was cancelled by the system, for example because of a billing problem',
        self::CANCELLED_BY_SUBSCRIPTION => 'Subscription was replaced with a new subscription',
    ];

    /**
     * @param int        $userId
     * @param Collection $transaction
     * @return bool
     */
    public static function validate(int $userId, Collection $transaction): bool
    {
        if (empty($transaction->get('transaction')->get('purchaseToken'))) {
            throw new EmptyAndroidPurchaseTokenException();
        }
        $googleAndroidPublisher = static::getPublisher();
        $validator = new \ReceiptValidator\GooglePlay\Validator($googleAndroidPublisher);

        $receipt = static::decodeReceipt($transaction);
        $packageName = $receipt->packageName;
        $productId = $receipt->productId;
        $token = $transaction->get('transaction')->get('purchaseToken');

        $response = $validator->setPackageName(static::APP_PACKAGE_NAME)
                              ->setProductId($productId)
                              ->setPurchaseToken($token)
                              ->validateSubscription();

        return $response->getPaymentState() === static::PAYMENT_RECEIVED || $response->getPaymentState() === static::PAYMENT_FREE_TRIAL;
    }

    /**
     * @param Collection $transaction
     * @return \stdClass
     */
    public static function decodeReceipt(Collection $transaction)
    {
        return json_decode($transaction->get('transaction')->get('receipt'));
    }

    /**
     * @param Payment $payment
     * @return bool
     */
    public static function refund(Payment $payment): bool
    {
        return static::getPublisher()->purchases_subscriptions->refund(
            static::APP_PACKAGE_NAME,
            $payment->getProviderId(),
            $payment->getToken()
        );
    }

    /**
     * @param Payment $payment
     * @return bool
     */
    public static function cancel(Payment $payment): bool
    {
        return static::getPublisher()->purchases_subscriptions->cancel(
            static::APP_PACKAGE_NAME,
            $payment->getProviderId(),
            $payment->getToken()
        );
    }

    /**
     * @param Payment $payment
     * @return \Google_Service_AndroidPublisher_SubscriptionPurchase
     */
    public static function get(Payment $payment)
    {
        return static::getPublisher()->purchases_subscriptions->get(
            static::APP_PACKAGE_NAME,
            $payment->getPlan()->getProviderId(),
            $payment->getToken()
        );
    }

    /**
     * @param \Google_Service_AndroidPublisher_SubscriptionPurchase $subscription
     *
     * @return bool
     */
    public static function isCancelled($subscription): bool
    {
        if (empty($subscription->getUserCancellationTimeMillis())) {
            return false;
        }

        $date = new \DateTime('@'.round($subscription->getUserCancellationTimeMillis() / 1000));
        $expiredDate = new \DateTime('@'.round($subscription->getExpiryTimeMillis() / 1000));

        return new \DateTime() >= $date
            && !$subscription->getAutoRenewing()
            && !is_null($subscription->getCancelReason())
            && new \DateTime() >= $expiredDate;
    }

    /**
     * @return \Google_Service_AndroidPublisher
     */
    public static function getPublisher(): \Google_Service_AndroidPublisher
    {
        return new \Google_Service_AndroidPublisher(static::getClient());
    }

    /**
     * @return \Google_Client
     */
    public static function getClient()
    {
        return tap(new \Google_Client(), function ($googleClient) {
            $googleClient->setScopes([\Google_Service_AndroidPublisher::ANDROIDPUBLISHER]);
            $googleClient->setApplicationName(static::APP_NAME);
            $googleClient->setAuthConfig(static::KEY_PATH);
        });
    }

    /**
     * @param Collection $transaction
     *
     * @return \DateTime
     */
    public static function getCompletedTimeFromPayload(Collection $transaction)
    {
        return new \DateTime();
    }

    /**
     * @param int $reasonId
     * @return null|string
     */
    public static function getReasonById(int $reasonId): ?string
    {
        if (array_key_exists($reasonId, static::CANCEL_REASONS)) {
            return static::CANCEL_REASONS[$reasonId];
        }

        return null;
    }
}
