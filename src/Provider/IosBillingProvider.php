<?php

namespace Fitatu\BillingBundle\Provider;

use Fitatu\ApiServerBundle\Util\Env;
use Fitatu\BillingBundle\Model\IosNotification;
use Fitatu\DatabaseBundle\Entity\Auth\Payment;
use Fitatu\BillingBundle\Exception\CancelNotSupportedException;
use Fitatu\BillingBundle\Exception\RefundsNotSupportedException;
use Illuminate\Support\Collection;
use ReceiptValidator\iTunes\Validator as iTunesValidator;
use Fitatu\BillingBundle\Exception\EmptyIOSReceiptException;
use Symetria\SharedBundle\Mapper\UserSystemMapper;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class IosBillingProvider implements BillingProviderInterface
{
    private const CANCELLED_NOTIFICATION_TYPE = "CANCELLED";
    /**
     * @param int        $userId
     * @param Collection $transaction
     *
     * @return bool
     *
     * @throws EmptyIOSReceiptException
     * @throws \ReceiptValidator\RunTimeException
     */
    public static function validate(int $userId, Collection $transaction): bool
    {
        $transaction = $transaction->get('transaction');
        $transactionReceipt = $transaction->get('transactionReceipt');
        $appStoreReceipt = $transaction->get('appStoreReceipt');

        if (empty($transactionReceipt) && empty($appStoreReceipt)) {
            throw new EmptyIOSReceiptException();
        }

        $receipt = $transactionReceipt ?: $appStoreReceipt;

        return static::isReceiptValid($receipt);
    }

    /**
     * @param Payment $payment
     * @throws RefundsNotSupportedException
     * @return bool
     */
    public static function refund(Payment $payment): bool
    {
        throw new RefundsNotSupportedException(
            UserSystemMapper::IOS
        );
    }

    /**
     * @param IosNotification $iosNotification
     * @return bool
     */
    public static function isCancelled(IosNotification $iosNotification): bool
    {
        return $iosNotification->getNotificationType() == static::CANCELLED_NOTIFICATION_TYPE && $iosNotification->getCancellationDateMs() && $iosNotification->getCancellationDate() >= new DateTime();
    }

    /**
     * @param Collection $transaction
     *
     * @return \DateTime
     */
    public static function getCompletedTimeFromPayload(Collection $transaction)
    {
        return new \DateTime();
    }

    /**
     * @param array $request
     * @return IosNotification
     */
    public static function consumeNotification(array $request): IosNotification
    {
        return IosNotification::create($request);
    }

    /**
     * @param Payment $payment
     * @return bool
     */
    public static function cancel(Payment $payment): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public static function getSharedSecret(): string
    {
        return Env::get('IOS_BILLING_SHARED_SECRET');
    }

    /**
     * @param string $receipt
     * @return bool
     */
    public static function isReceiptValid(string $receipt): bool
    {
        $endpoint = Env::get('IOS_BILLING_SANDBOX') ? iTunesValidator::ENDPOINT_SANDBOX : iTunesValidator::ENDPOINT_PRODUCTION;
        $validator = new iTunesValidator($endpoint);

        return $validator->setReceiptData($receipt)
                  ->setSharedSecret(static::getSharedSecret())
                  ->validate()
                  ->isValid();
    }
}
