<?php

namespace Fitatu\BillingBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Fitatu\BillingBundle\DependencyInjection\FitatuBillingExtension;
/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class FitatuBillingBundle extends Bundle
{
    /**
     * @return FitatuBillingExtension
     */
    public function getContainerExtension()
    {
        return new FitatuBillingExtension();
    }
}
