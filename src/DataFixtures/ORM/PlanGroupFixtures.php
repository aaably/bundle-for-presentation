<?php

namespace Fitatu\BillingBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Fitatu\DatabaseBundle\Entity\Auth\PlanGroup;

/**
 * @author Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class PlanGroupFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    const PLAN_DURATION = 30;

    /**
     * @param ObjectManager $manager
     *
     * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     */
    public function load(ObjectManager $manager)
    {
        $planGroup1 = new PlanGroup('1 Monthly Diet');
        $planGroup1->setDuration(static::PLAN_DURATION)
                   ->setType(PlanGroup::TYPE_DIET);
        $manager->persist($planGroup1);
        $this->addReference('plan-group-1', $planGroup1);

        $planGroup2 = new PlanGroup('3 Monthly Diet');
        $planGroup2->setDuration(90)
                   ->setType(PlanGroup::TYPE_DIET);

        $manager->persist($planGroup2);
        $this->addReference('plan-group-2', $planGroup2);

        $planGroup3 = new PlanGroup('3 Monthly Premium');
        $planGroup3->setDuration(90)
            ->setType(PlanGroup::TYPE_PREMIUM);

        $manager->persist($planGroup3);
        $this->addReference('plan-group-3', $planGroup3);

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 1;
    }
}
