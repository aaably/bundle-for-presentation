<?php

namespace Fitatu\BillingBundle\DataFixtures\ORM;

use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Fitatu\DatabaseBundle\Entity\Auth\Plan;
use Fitatu\DatabaseBundle\Entity\Auth\Subscription;
use Symetria\SharedBundle\Model\UserInterface as User;

/**
 * @author Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class SubscriptionFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     *
     * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     */
    public function load(ObjectManager $manager)
    {
        /** @var User $user */
        $user = $this->getReference('user-1');

        /** @var User $user2 */
        $user2 = $this->getReference('user-2');

        /** @var Plan $plan1 */
        $plan1 = $this->getReference('plan-1');

        /** @var Plan $plan2 */
        $plan2 = $this->getReference('plan-2');

        // Waiting to activate
        $subscription1 = new Subscription($user, $plan1);
        $subscription1
            ->setActive(false)
            ->setStartDate(new DateTime('2017-02-01'))
            ->setEndDate(new DateTime('2300-12-31'));

        $manager->persist($subscription1);
        $this->addReference('subscription-1', $subscription1);

        // Waiting to disactivate
        $subscription2 = new Subscription($user, $plan2);
        $subscription2
            ->setActive(true)
            ->setStartDate(new DateTime('2016-01-01'))
            ->setEndDate(new DateTime('2017-01-01'));

        $manager->persist($subscription2);
        $this->addReference('subscription-2', $subscription2);

        // Active
        $subscription3 = new Subscription($user, $plan1);
        $subscription3
            ->setActive(true)
            ->setStartDate(new DateTime('2017-02-01'))
            ->setEndDate(new DateTime('2300-12-31'));

        $manager->persist($subscription3);
        $this->addReference('subscription-3', $subscription3);

        $subscription4 = new Subscription($user2, $plan1);
        $subscription4
            ->setActive(true)
            ->setStartDate(new DateTime('2017-02-01'))
            ->setEndDate(new DateTime('2300-12-31'));

        $manager->persist($subscription4);
        $this->addReference('subscription-4', $subscription4);

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 1;
    }
}
