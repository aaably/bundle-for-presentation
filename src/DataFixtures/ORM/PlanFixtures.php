<?php

namespace Fitatu\BillingBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Fitatu\DatabaseBundle\Entity\Auth\Plan;
use Fitatu\DatabaseBundle\Entity\Auth\PlanGroup;

/**
 * @author Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class PlanFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    const PROVIDER_1_ID = "uniqueProviderId";
    const PROVIDER_2_ID = "uniqueProviderId2";

    /**
     * @param ObjectManager $manager
     *
     * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     */
    public function load(ObjectManager $manager)
    {
        /** @var PlanGroup $planGroup1 */
        $planGroup1 = $this->getReference('plan-group-1');

        /** @var PlanGroup $planGroup2 */
        $planGroup2 = $this->getReference('plan-group-2');

        /** @var PlanGroup $planGroup3 */
        $planGroup3 = $this->getReference('plan-group-3');

        $plan1 = new Plan($planGroup1);
        $plan1->setLocale('pl_PL')->setCurrency('PLN')->setAmount('99.00')
              ->setProviderId(static::PROVIDER_1_ID)
              ->setIsActive(true)
              ->setProvider(2);
        $manager->persist($plan1);
        $this->addReference('plan-1', $plan1);

        $plan1 = new Plan($planGroup1);
        $plan1->setLocale('fr_FR')->setCurrency('EUR')->setAmount('29.00')
              ->setIsActive(true)
              ->setProviderId(static::PROVIDER_1_ID)
              ->setProvider(2);
        $manager->persist($plan1);
        $this->addReference('plan-1-fr', $plan1);

        $plan1 = new Plan($planGroup1);
        $plan1->setLocale('pl_PL')->setCurrency('PLN')->setAmount('29.00')
              ->setProviderId(static::PROVIDER_1_ID)
              ->setIsActive(true)
              ->setProvider(1);
        $manager->persist($plan1);
        $this->addReference('plan-1-prov-1', $plan1);

        $plan2 = new Plan($planGroup2);
        $plan2->setLocale('pl_PL')->setCurrency('PLN')->setAmount('129.00')
              ->setIsActive(true)
              ->setProvider(2);
        $manager->persist($plan2);
        $this->addReference('plan-2', $plan2);

        $plan3 = new Plan($planGroup2);
        $plan3->setLocale('en_GB')
              ->setIsActive(true)
              ->setCurrency('USD')->setAmount('31.00')->setProvider(2);
        $manager->persist($plan3);
        $this->addReference('plan-3', $plan3);

        $plan4 = new Plan($planGroup2);
        $plan4->setLocale('fr_FR')->setCurrency('USD')
              ->setIsActive(true)
              ->setAmount('30.00')->setProvider(1)
              ->setProviderId('abcdef');
        $manager->persist($plan4);
        $this->addReference('plan-4', $plan4);

        $plan5 = new Plan($planGroup3);
        $plan5->setLocale('fr_FR')->setCurrency('USD')
            ->setIsActive(true)
            ->setAmount('30.00')->setProvider(1)
            ->setProviderId('abcdef');
        $manager->persist($plan5);
        $this->addReference('plan-premium-1', $plan5);

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 1;
    }
}
