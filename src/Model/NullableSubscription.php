<?php

namespace Fitatu\BillingBundle\Model;

use Fitatu\DatabaseBundle\Entity\Auth\Plan;
use Fitatu\DatabaseBundle\Entity\Auth\PlanGroup;
use Fitatu\DatabaseBundle\Entity\Auth\Subscription;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class NullableSubscription extends Subscription
{
    public function __construct()
    {
        parent::__construct(
            new NullableUser(),
            new Plan(
                new PlanGroup('')
            )
        );
    }

    /**
     * @return Subscription
     */
    public static function create(): Subscription
    {
        return new static;
    }
}
