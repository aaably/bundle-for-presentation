<?php

namespace Fitatu\BillingBundle\Model;

use Fitatu\SharedUserBundle\Model\User\UserEntity\UserAppTrait;
use Fitatu\SharedUserBundle\Model\User\UserEntity\UserGroupsTrait;
use Fitatu\SharedUserBundle\Model\User\UserEntity\UserMetaTrait;
use Fitatu\SharedUserBundle\Model\User\UserEntity\UserSettingsTrait;
use Fitatu\SharedUserBundle\Model\User\UserEntity\UserSocialTrait;
use Fitatu\SharedUserBundle\Model\User\UserEntity\UserTermsTrait;
use Fitatu\SharedUserBundle\Model\User\UserEntityTrait;
use Fitatu\SharedUserBundle\Model\User\UserSharedPropertiesTrait;
use Fitatu\SharedUserBundle\Model\User\UserTokenTrait;
use Symetria\SharedBundle\Model\UserInterface;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class NullableUser implements UserInterface
{
    use UserAppTrait,
        UserEntityTrait,
        UserGroupsTrait,
        UserMetaTrait,
        UserSettingsTrait,
        UserSharedPropertiesTrait,
        UserSocialTrait,
        UserTermsTrait,
        UserTokenTrait;

    /**
     * @return string
     */
    public function __toString()
    {
        return '';
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return 123;
    }

    /**
     * This declaration does not have a strictly defined return type because it needs to be compatible
     * with \Symfony\Component\Security\Core\User\UserInterface
     *
     * @return string
     */
    public function getUsername(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return '';
    }

    /**
     * This declaration does not have a strictly defined return type because it needs to be compatible
     * with \Symfony\Component\Security\Core\User\UserInterface
     *
     * @return array
     */
    public function getRoles(): array
    {
        return [];
    }

    /**
     * This declaration does not have a strictly defined return type because it needs to be compatible
     * with \Symfony\Component\Security\Core\User\AdvancedUserInterface
     *
     * @return bool
     */
    public function isEnabled(): bool
    {
        return true;
    }
}