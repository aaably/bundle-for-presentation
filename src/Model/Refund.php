<?php

namespace Fitatu\BillingBundle\Model;

use Fitatu\DatabaseBundle\Entity\Auth\Payment;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class Refund
{
    /**
     * @var Payment
     */
    private $payment;

    /**
     * @var bool
     */
    private $success;

    /**
     * @var string|null
     */
    private $message;

    /**
     * @param Payment $payment
     * @param bool    $success
     */
    public function __construct(Payment $payment, bool $success)
    {
        $this->payment = $payment;
        $this->success = $success;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     * @return Refund
     */
    public function setSuccess(bool $success): Refund
    {
        $this->success = $success;
    }

    /**
     * @return Payment
     */
    public function getPayment(): Payment
    {
        return $this->payment;
    }

    /**
     * @param Payment $payment
     */
    public function setPayment(Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'isSuccess' => $this->isSuccess(),
            'payment'   => $this->getPayment(),
        ];
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return Refund
     */
    public function setMessage(string $message): Refund
    {
        $this->message = $message;

        return $this;
    }
}
