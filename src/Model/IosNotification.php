<?php

namespace Fitatu\BillingBundle\Model;

use DateTime;
use Fitatu\SharedBundle\Model\ModelPropertiesSetter;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class IosNotification
{
    /**
     * @var string|null
     */
    protected $latestReceipt;

    /**
     * @var string|null
     */
    protected $environment;

    /**
     * @var bool
     */
    protected $autoRenewStatus = true;

    /**
     * @var string|null
     */
    protected $password;

    /**
     * @var string|null
     */
    protected $notificationType;

    /**
     * @var string|null
     */
    protected $autoRenewProductId;

    /**
     * @var int|null
     */
    protected $quantity;

    /**
     * @var string|null
     */
    protected $uniqueVendorIdentifier;

    /**
     * @var string|null
     */
    protected $originalPurchaseDatePst;

    /**
     * @var DateTime|null
     */
    protected $originalPurchaseDate;

    /**
     * @var string|null
     */
    protected $expiresDateFormatted;

    /**
     * @var bool|null
     */
    protected $isInIntroOfferPeriod;

    /**
     * @var int|null
     */
    protected $purchaseDateMs;

    /**
     * @var string|null
     */
    protected $expiresDateFormattedPst;

    /**
     * @var bool
     */
    protected $isTrialPeriod;

    /**
     * @var int|null
     */
    protected $itemId;

    /**
     * @var string|null
     */
    protected $uniqueIdentifier;

    /**
     * @var DateTime
     */
    protected $originalTransactionId;

    /**
     * @var DateTime|null
     */
    protected $expiresDate;

    /**
     * @var int|null
     */
    protected $appItemId;

    /**
     * @var int|null
     */
    protected $transactionId;

    /**
     * @var string|null
     */
    protected $bvrs;

    /**
     * @var int|null
     */
    protected $webOrderLineItemId;

    /**
     * @var int|null
     */
    protected $versionExternalIdentifier;

    /**
     * @var string|null
     */
    protected $bid;

    /**
     * @var string|null
     */
    protected $productId;

    /**
     * @var DateTime|null
     */
    protected $purchaseDate;

    /**
     * @var string|null
     */
    protected $purchaseDatePst;

    /**
     * @var int|null
     */
    protected $originalPurchaseDateMs;

    /**
     * @var int|null
     */
    private $cancellationDateMs;

    /**
     * @var DateTime|null
     */
    private $cancellationDate;

    /**
     * @param array $attributes
     * @return IosNotification
     */
    public static function create(array $attributes): IosNotification
    {
        $attributes = collect($attributes);
        $expiredReceipt = collect($attributes->pull('latest_expired_receipt_info'));
        $receipt = collect($attributes->pull('latest_receipt_info'));

        $attributes = $attributes->merge($receipt)->merge($expiredReceipt);

        $attributes = $attributes->mapWithKeys(function ($value, $key) {
            $key = str_replace('_', '', ucwords($key, '_'));

            return [
                $key => $value,
            ];
        });

        return ModelPropertiesSetter::set(new IosNotification(), $attributes->toArray());
    }

    /**
     * @return string|null
     */
    public function getLatestReceipt(): ?string
    {
        return $this->latestReceipt;
    }

    /**
     * @param string $latestReceipt
     * @return IosNotification
     */
    public function setLatestReceipt(string $latestReceipt): IosNotification
    {
        $this->latestReceipt = $latestReceipt;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getOriginalPurchaseDate()
    {
        return $this->originalPurchaseDate;
    }

    /**
     * @return string|null
     */
    public function getEnvironment(): ?string
    {
        return $this->environment;
    }

    /**
     * @param string $environment
     * @return IosNotification
     */
    public function setEnvironment(string $environment): IosNotification
    {
        $this->environment = $environment;

        return $this;
    }

    /**
     * @return bool
     */
    public function getAutoRenewStatus(): bool
    {
        return $this->autoRenewStatus;
    }

    /**
     * @param string $autoRenewStatus
     * @return IosNotification
     */
    public function setAutoRenewStatus(string $autoRenewStatus): IosNotification
    {
        $this->autoRenewStatus = json_decode($autoRenewStatus);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return IosNotification
     */
    public function setPassword(string $password): IosNotification
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNotificationType(): ?string
    {
        return $this->notificationType;
    }

    /**
     * @param string $notificationType
     * @return IosNotification
     */
    public function setNotificationType(string $notificationType): IosNotification
    {
        $this->notificationType = $notificationType;

        return $this;
    }

    /**
     * @param string $autoRenewProductId
     * @return IosNotification
     */
    public function setAutoRenewProductId(string $autoRenewProductId): IosNotification
    {
        $this->autoRenewProductId = $autoRenewProductId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAutoRenewProductId(): ?string
    {
        return $this->autoRenewProductId;
    }

    /**
     * @return string|null
     */
    public function getUniqueVendorIdentifier(): ?string
    {
        return $this->uniqueVendorIdentifier;
    }

    /**
     * @param string $uniqueVendorIdentifier
     * @return IosNotification
     */
    public function setUniqueVendorIdentifier(string $uniqueVendorIdentifier): IosNotification
    {
        $this->uniqueVendorIdentifier = $uniqueVendorIdentifier;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getOriginalPurchaseDatePst(): ?DateTime
    {
        return $this->originalPurchaseDatePst;
    }

    /**
     * @param string $originalPurchaseDatePst
     * @return IosNotification
     */
    public function setOriginalPurchaseDatePst(string $originalPurchaseDatePst): IosNotification
    {
        $this->originalPurchaseDatePst = $originalPurchaseDatePst;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOriginalPurchaseDateMs()
    {
        return $this->originalPurchaseDateMs;
    }

    /**
     * @param int $originalPurchaseDateMs
     * @return IosNotification
     */
    public function setOriginalPurchaseDateMs(int $originalPurchaseDateMs): IosNotification
    {
        $this->originalPurchaseDate = new DateTime('@'.$originalPurchaseDateMs/1000);
        $this->originalPurchaseDateMs = $originalPurchaseDateMs;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getExpiresDateFormatted()
    {
        return $this->expiresDateFormatted;
    }

    /**
     * @
     * @param string $expiresDateFormatted
     * @return IosNotification
     */
    public function setExpiresDateFormatted(string $expiresDateFormatted): IosNotification
    {
        $this->expiresDateFormatted = $expiresDateFormatted;

        return $this;
    }

    /**
     * @return mixed
     */
    public function isInIntroOfferPeriod()
    {
        return $this->isInIntroOfferPeriod;
    }

    /**
     * @param string $isInIntroOfferPeriod
     * @return IosNotification
     */
    public function setIsInIntroOfferPeriod(string $isInIntroOfferPeriod): IosNotification
    {
        $this->isInIntroOfferPeriod = json_decode($isInIntroOfferPeriod);

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getPurchaseDate()
    {
        return $this->purchaseDate;
    }

    /**
     * @return int|null
     */
    public function getPurchaseDateMs(): ?int
    {
        return $this->purchaseDateMs;
    }

    /**
     * @param int $purchaseDateMs
     * @return IosNotification
     */
    public function setPurchaseDateMs(int $purchaseDateMs): IosNotification
    {
        $this->purchaseDate = new DateTime( '@'.$purchaseDateMs/1000);
        $this->purchaseDateMs = $purchaseDateMs;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getExpiresDateFormattedPst(): ?string
    {
        return $this->expiresDateFormattedPst;
    }

    /**
     * @
     * @param string $expiresDateFormattedPst
     * @return IosNotification
     */
    public function setExpiresDateFormattedPst(string $expiresDateFormattedPst): IosNotification
    {
        $this->expiresDateFormattedPst = $expiresDateFormattedPst;

        return $this;
    }

    /**
     * @return bool
     */
    public function isTrialPeriod(): bool
    {
        return $this->isTrialPeriod;
    }

    /**
     * @param string $isTrialPeriod
     * @return IosNotification
     */
    public function setIsTrialPeriod(string $isTrialPeriod): IosNotification
    {
        $this->isTrialPeriod = json_decode($isTrialPeriod);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getOriginalTransactionId(): ?int
    {
        return $this->originalTransactionId;
    }

    /**
     * @param int $originalTransactionId
     * @return IosNotification
     */
    public function setOriginalTransactionId(int $originalTransactionId): IosNotification
    {
        $this->originalTransactionId = $originalTransactionId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getItemId(): ?int
    {
        return $this->itemId;
    }

    /**
     * @param int $itemId
     * @return IosNotification
     */
    public function setItemId(int $itemId): IosNotification
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * @return string
     */
    public function getPurchaseDatePst(): string
    {
        return $this->purchaseDatePst;
    }

    /**
     * @param string $purchaseDatePst
     * @return IosNotification
     */
    public function setPurchaseDatePst(string $purchaseDatePst): IosNotification
    {
        $this->purchaseDatePst = $purchaseDatePst;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUniqueIdentifier(): ?string
    {
        return $this->uniqueIdentifier;
    }

    /**
     * @param string $uniqueIdentifier
     * @return IosNotification
     */
    public function setUniqueIdentifier(string $uniqueIdentifier): IosNotification
    {
        $this->uniqueIdentifier = $uniqueIdentifier;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getExpiresDate(): ?DateTime
    {
        return $this->expiresDate;
    }

    /**
     * @param int $expiresDate
     * @return IosNotification
     */
    public function setExpiresDate(int $expiresDate): IosNotification
    {
        $this->expiresDate = new DateTime('@'.round($expiresDate/1000));

        return $this;
    }

    /**
     * @return int|null
     */
    public function getAppItemId(): ?int
    {
        return $this->appItemId;
    }

    /**
     * @param int $appItemId
     * @return IosNotification
     */
    public function setAppItemId(int $appItemId): IosNotification
    {
        $this->appItemId = $appItemId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getTransactionId(): ?int
    {
        return $this->transactionId;
    }

    /**
     * @param int $transactionId
     * @return IosNotification
     */
    public function setTransactionId(int $transactionId): IosNotification
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBvrs(): ?string
    {
        return $this->bvrs;
    }

    /**
     * @param string $bvrs
     * @return IosNotification
     */
    public function setBvrs(string $bvrs): IosNotification
    {
        $this->bvrs = $bvrs;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getWebOrderLineItemId(): ?int
    {
        return $this->webOrderLineItemId;
    }

    /**
     * @param int $webOrderLineItemId
     * @return IosNotification
     */
    public function setWebOrderLineItemId(int $webOrderLineItemId): IosNotification
    {
        $this->webOrderLineItemId = $webOrderLineItemId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getVersionExternalIdentifier(): ?int
    {
        return $this->versionExternalIdentifier;
    }

    /**
     * @param int $versionExternalIdentifier
     * @return IosNotification
     */
    public function setVersionExternalIdentifier(int $versionExternalIdentifier): IosNotification
    {
        $this->versionExternalIdentifier = $versionExternalIdentifier;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBid(): ?string
    {
        return $this->bid;
    }

    /**
     * @param string $bid
     * @return IosNotification
     */
    public function setBid(string $bid): IosNotification
    {
        $this->bid = $bid;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProductId(): ?string
    {
        return $this->productId;
    }

    /**
     * @param string $productId
     * @return $this
     */
    public function setProductId(string $productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param string $quantity
     * @return IosNotification
     */
    public function setQuantity(string $quantity): IosNotification
    {
        $this->quantity = (int)$quantity;

        return $this;
    }

    /**
     * @param int $cancellationDateMs
     * @return IosNotification
     */
    public function setCancellationDateMs(int $cancellationDateMs): IosNotification
    {
       $this->cancellationDateMs = $cancellationDateMs;
       $this->cancellationDate = new DateTime('@'.round($cancellationDateMs/1000));

       return $this;
    }

    /**
     * @return int|null
     */
    public function getCancellationDateMs(): ?int
    {
        return $this->cancellationDateMs;
    }

    /**
     * @return DateTime|null
     */
    public function getCancellationDate(): ?DateTime
    {
        return $this->cancellationDate;
    }
}