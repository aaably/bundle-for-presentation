<?php

namespace Fitatu\BillingBundle\Factory;

use Fitatu\BillingBundle\Provider\BillingProvider;
use Fitatu\BillingBundle\Provider\BillingProviderInterface;
use Fitatu\DatabaseBundle\Entity\Auth\Subscription;
use Illuminate\Support\Collection;
use Symetria\SharedBundle\Model\UserInterface;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PaymentFactory
{
    /**
     * @param UserInterface $user
     * @param Subscription  $subscription
     * @param Collection    $transaction
     *
     * @return array
     */
    public static function create(UserInterface $user, Subscription $subscription, Collection $transaction): array
    {
        $type = $transaction->get('transaction')->get('type');
        /** @var BillingProviderInterface $provider */
        $provider = BillingProvider::getByTransactionType($type);

        return [
            'provider'     => $type,
            'providerId'   => $transaction->get('transaction')->get('id'),
            'token'        => $transaction->get('transaction')->get('purchaseToken'),
            'currency'     => $transaction->get('currency'),
            'amount'       => (int)filter_var($transaction->get('price'), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION) / 100,
            'plan'         => $subscription->getPlan(),
            'subscription' => $subscription,
            'user'         => $user,
            'completedAt'  => $provider->getCompletedTimeFromPayload($transaction)
        ];
    }
}
