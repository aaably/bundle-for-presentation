<?php

namespace Fitatu\BillingBundle\Factory;

use Illuminate\Support\Collection;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PaymentLogFactory
{
    /**
     * @param int        $userId
     * @param Collection $transaction
     * @return string[]
     */
    public static function create(int $userId, Collection $transaction): array
    {
        $type = $transaction->get('transaction') ? $transaction->get('transaction')->get('type') : '';
        return [
            'user_id'          => $userId,
            'currency'         => $transaction->get('currency'),
            'state'            => $transaction->get('state'),
            'price'            => (int)$transaction->get('price'),
            'product_id'       => $transaction->get('id'),
            'product_type'     => $transaction->get('type'),
            'type'             => $type,
            'transaction_id'   => (int)'', //fixme
            'transaction_path' => $transaction->get('transactions'),
//            'valid'            => (bool)$transaction->get('valid')
        ];
    }
}
