<?php

namespace Fitatu\BillingBundle\Service;

use DateInterval;
use DateTime;
use DateTimeInterface;
use Fitatu\BillingBundle\Exception\NoActiveSubscriptionException;
use Fitatu\BillingBundle\Exception\SubscriptionPlanGroupNotFoundException;
use Fitatu\BillingBundle\Exception\SubscriptionPlanNotFoundException;
use Fitatu\BillingBundle\Exception\UserNotFoundException;
use Fitatu\BillingBundle\Model\NullableSubscription;
use Fitatu\DatabaseBundle\Entity\Auth\Plan;
use Fitatu\DatabaseBundle\Entity\Auth\PlanGroup;
use Fitatu\DatabaseBundle\Entity\Auth\Subscription;
use Fitatu\DatabaseBundle\Repository\Auth\AbstractPlanRepository;
use Fitatu\DatabaseBundle\Repository\Auth\AbstractSubscriptionRepository;
use Fitatu\SharedUserBundle\Repository\RoleRepositoryInterface;
use Fitatu\SharedUserBundle\Repository\UserRepositoryInterface;
use LogicException;
use Symetria\SharedBundle\Model\UserInterface as User;

/**
 * @author    Lukasz Domanski
 * @copyright Fitatu Sp. z o.o.
 */
class SubscriptionService
{
    /**
     * @var AbstractSubscriptionRepository
     */
    private $subscriptionRepository;

    /**
     * @var RoleRepositoryInterface
     */
    private $roleRepository;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var AbstractPlanRepository
     */
    private $planRepository;

    /**
     * @param AbstractSubscriptionRepository  $subscriptionRepository
     * @param AbstractPlanRepository          $planRepository
     */
    public function __construct(
        AbstractSubscriptionRepository $subscriptionRepository,
        AbstractPlanRepository $planRepository
    ) {
        $this->subscriptionRepository = $subscriptionRepository;
        $this->planRepository = $planRepository;
    }

    /**
     * @param UserRepositoryInterface $userRepository
     */
    public function setUserRepository(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param RoleRepositoryInterface $roleRepository
     */
    public function setRoleRepository(RoleRepositoryInterface $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * @param int $subscriptionId
     *
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function activate(int $subscriptionId)
    {
        $subscription = $this->subscriptionRepository->find($subscriptionId);
        if (!$subscription instanceof Subscription) {
            throw new LogicException(sprintf('Subscription not found: #%d', $subscriptionId));
        }

        $subscription->setActive(true);
        $this->subscriptionRepository->persist($subscription);

        $this->addPlanRoles($subscription->getUser(), $subscription->getPlan());

        // todo Do another operations e.g. send an email, send a notification
    }

    /**
     * @param int $subscriptionId
     *
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deactivate(int $subscriptionId)
    {
        $subscription = $this->subscriptionRepository->find($subscriptionId);
        if (!$subscription instanceof Subscription) {
            throw new LogicException(sprintf('Subscription not found: #%d', $subscriptionId));
        }

        $subscription->setActive(false);
        $this->subscriptionRepository->persist($subscription);

        $this->removePlanRoles($subscription->getUser(), $subscription->getPlan());

        // todo Do another operations e.g. send an email, send a notification
    }

    /**
     * @param int|User          $user
     * @param int|Plan          $plan
     * @param DateTimeInterface $fromDate
     * @param DateTimeInterface $toDate
     *
     * @return Subscription
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function subscribeUserToPlan($user, $plan, $fromDate = null, $toDate = null)
    {
        if (is_int($plan)) {
            /** @var Plan $plan */
            $plan = $this->planRepository->find($plan);
        }
        if (is_int($user)) {
            /** @var User $user */
            $user = $this->userRepository->find($user);
        }

        if (empty($user)) {
            throw new UserNotFoundException($user);
        }
        if (empty($plan)) {
            throw new SubscriptionPlanNotFoundException($plan);
        }

        list($fromDate, $toDate) = $this->createDatesScope(
            $fromDate,
            $toDate,
            $plan->getPlanGroup()->getDuration()
        );

        return $this->dispatch($user, $plan, $fromDate, $toDate);
    }

    /**
     * @param int[]             $users
     * @param int|Plan          $plan
     * @param DateTimeInterface $fromDate
     * @param DateTimeInterface $toDate
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function subscribeUsersToPlan(
        array $users,
        $plan,
        DateTimeInterface $fromDate = null,
        DateTimeInterface $toDate = null
    ) {
        $users = $this->userRepository->findBy(['id' => $users]);

        foreach ($users as $user) {
            $this->subscribeUserToPlan($user, $plan, $fromDate, $toDate);
        }
    }

    /**
     * @param int                    $userId
     * @param int                    $planGroupId
     * @param string|null            $locale
     * @param DateTimeInterface|null $fromDate
     * @param DateTimeInterface|null $toDate
     *
     * @return Subscription
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function subscribeUserToPlanGroup(
        int $userId,
        int $planGroupId,
        string $locale = null,
        DateTimeInterface $fromDate = null,
        DateTimeInterface $toDate = null
    ) {
        /** @var Plan $plan */
        /** @var User $user */
        $user = $this->userRepository->find($userId);
        if (is_null($locale)) {
            $locale = $user->getLocale();
        }

        $plan = $this->getPlanFromPlanGroup($planGroupId, $locale);

        if (empty($user)) {
            throw new UserNotFoundException($userId);
        }
        if (empty($plan)) {
            throw new SubscriptionPlanGroupNotFoundException($planGroupId);
        }

        list($fromDate, $toDate) = $this->createDatesScope(
            $fromDate,
            $toDate,
            $plan->getPlanGroup()->getDuration()
        );

        return $this->dispatch($user, $plan, $fromDate, $toDate);
    }

    /**
     * @param int $userId
     *
     * @return Subscription
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function cancelUserSubscription(int $userId)
    {
        /** @var Subscription $subscription */
        $subscription = $this->subscriptionRepository->findOneBy(['user' => $userId]);

        if (empty($subscription)) {
            throw new NoActiveSubscriptionException();
        }

        return $this->subscriptionRepository->cancel($subscription);
    }

    /**
     * @param DateTimeInterface|null $fromDate
     * @param DateTimeInterface|null $toDate
     * @param int                    $days
     *
     * @return array
     */
    public function createDatesScope(
        DateTimeInterface $fromDate = null,
        DateTimeInterface $toDate = null,
        int $days
    ): array {
        if (empty($fromDate)) {
            $fromDate = new DateTime();
        }

        if (empty($toDate)) {
            $toDate = (new DateTime())->add(
                new DateInterval('P'.$days.'D')
            );
        }

        return [$fromDate, $toDate];
    }

    /**
     * @param int                    $userId
     * @param DateTimeInterface|null $date
     *
     * @return bool
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function isActive(int $userId, DateTimeInterface $date = null): bool
    {
        return $this->subscriptionRepository->isActive($userId, $date);
    }

    /**
     * Dispatch creation process.
     *
     * @param User              $user
     * @param Plan              $plan
     * @param DateTimeInterface $fromDate
     * @param DateTimeInterface $toDate
     *
     * @return Subscription
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function dispatch(User $user, Plan $plan, DateTimeInterface $fromDate, DateTimeInterface $toDate): Subscription
    {
        $subscription = $this->subscriptionRepository->createForUser($user, $plan, $fromDate, $toDate);
        $this->subscriptionRepository->persist($subscription);

        return $subscription;
    }

    /**
     * @param int $userId
     *
     * @return Subscription
     */
    public function get(int $userId): Subscription
    {
        $subscriptions = $this->subscriptionRepository->findActiveForUser($userId);

        if (empty($subscriptions) || !$subscriptions[0] instanceof Subscription) {
            return NullableSubscription::create();
        }

        return $subscriptions[0];
    }

    /**
     * @param int    $planGroupId
     * @param string $locale
     *
     * @return Plan|null
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getPlanFromPlanGroup(int $planGroupId, string $locale)
    {
        return $this->planRepository->findOneByGroupAndLocale($planGroupId, $locale);
    }

    /**
     * @param User $user
     * @param Plan $plan
     */
    private function addPlanRoles(User $user, Plan $plan)
    {
        foreach ($plan->getPlanGroup()->getRoles() as $role) {
            if (false === $user->hasRole($role)) {
                $user->addRole($role);
            }
        }
        $this->userRepository->persist($user);
    }

    /**
     * @param User $user
     * @param Plan $plan
     */
    private function removePlanRoles(User $user, Plan $plan)
    {
        foreach ($plan->getPlanGroup()->getRoles() as $role) {
            $user->removeRole($role);
        }
        $this->userRepository->persist($user);
    }

    /**
     * @param int $userId
     * @return DateTimeInterface|null
     */
    public function getDietSubscriptionEndDate(int $userId)
    {
        $subscription = $this->subscriptionRepository->findActiveSubscriptionForUser($userId, PlanGroup::TYPE_DIET);

        if (!$subscription instanceof Subscription) {
            return null;
        }

        if ($subscription->getCancelledAt() instanceof DateTimeInterface) {
            return $subscription->getEndDate();
        }

        return null;
    }
}
