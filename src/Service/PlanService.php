<?php

namespace Fitatu\BillingBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Fitatu\BillingBundle\Exception\BillingPlanNotFoundException;
use Fitatu\BillingBundle\Exception\PlanGroupTypeNotSupportedException;
use Fitatu\BillingBundle\Provider\BillingProvider;
use Fitatu\DatabaseBundle\Entity\Auth\Plan;
use Fitatu\DatabaseBundle\Entity\Auth\PlanGroup;
use Fitatu\DatabaseBundle\Repository\Auth\AbstractPlanRepository;
use Fitatu\SharedBundle\Util\Amount;
use Illuminate\Support\Collection;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PlanService
{
    /**
     * @var AbstractPlanRepository
     */
    private $planRepository;

    /**
     * @param AbstractPlanRepository $planRepository
     */
    public function __construct(AbstractPlanRepository $planRepository)
    {
        $this->planRepository = $planRepository;
    }

    /**
     * @param int $planId
     * @return array
     */
    public function get(int $planId): array
    {
        $plan = $this->planRepository->find($planId);

        if (!$plan instanceof Plan) {
            throw new BillingPlanNotFoundException();
        }

        return PlanFormatter::format($plan);
    }

    /**
     * @param string $locale
     * @param string $provider
     * @param string $type
     * @return Collection
     * @throws PlanGroupTypeNotSupportedException
     */
    public function getForLocaleByProvider(string $locale, string $provider, string $type = null): Collection
    {
        if (is_null($type)) {
            $type = PlanGroup::DEFAULT_TYPE_NAME;
        }

        $type = strtoupper($type);

        if (!array_key_exists($type, PlanGroup::TYPES)) {
            throw new PlanGroupTypeNotSupportedException($type);
        }

        $plans = $this->planRepository->getByLocaleAndProvider(
            $locale,
            BillingProvider::getId($provider),
            PlanGroup::TYPES[$type]
        );

        return $this->transformPlansCollection(
            $this->collectData($plans)
        );
    }

    /**
     * @param ArrayCollection $plans
     * @return array|Collection
     */
    public function collectData($plans): Collection
    {
        $collection = collect();

        foreach ($plans as $plan) {
            $collection[] = PlanFormatter::format($plan);
        }

        return $collection;
    }

    /**
     * @param Collection $collection
     * @return Collection
     */
    public function transformPlansCollection(Collection $collection): Collection
    {
        $monthlyPlan = $collection->last();

        $collection->transform(function ($plan) use ($monthlyPlan) {
            return $this->compareWithMonthlyPlan($plan, $monthlyPlan);
        });

        return $collection;
    }

    /**
     * @param array $plan
     * @param array $monthlyPlan
     * @return array
     */
    public function compareWithMonthlyPlan(array $plan, array $monthlyPlan): array
    {
        $amount = $plan['amount'];

        if (!$this->isMonthlyPlan($plan, $monthlyPlan)) {
            $amount['saving'] = (string)$this->calculateSavings($plan, $monthlyPlan);
        }
        $plan['amount'] = $amount;

        return $plan;
    }

    /**
     * @param array $plan
     * @param array $monthlyPlan
     * @return string
     */
    public function calculateSavings(array $plan, array $monthlyPlan): string
    {
        // savings = 30 days * mothly plan price in int - selected plan price in int
        $intAmount = (int)$plan['duration'] * $monthlyPlan['amount']['integer'] - $plan['amount']['integer'];

        return (new Amount($intAmount / 100))->getAsCurrency();
    }

    /**
     * @param array $plan
     * @param array $monthlyPlan
     * @return bool
     */
    public function isMonthlyPlan(array $plan, array $monthlyPlan): bool
    {
        return $plan['amount']['total'] == $monthlyPlan['amount']['total'];
    }
}
