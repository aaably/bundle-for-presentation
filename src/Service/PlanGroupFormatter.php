<?php

namespace Fitatu\BillingBundle\Service;

use Fitatu\DatabaseBundle\Entity\Auth\PlanGroup;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PlanGroupFormatter
{
    /**
     * @param PlanGroup $group
     * @return array
     */
    public static function format(PlanGroup $group): array
    {
        return PlanFormatter::format(
            $group->getPlans()->first()
        );
    }
}
