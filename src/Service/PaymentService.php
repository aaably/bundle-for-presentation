<?php

namespace Fitatu\BillingBundle\Service;

use Fitatu\ApiClientBundle\Service\UserCacheCleanerService;
use Fitatu\BillingBundle\Exception\TransactionDetailsNotProvidedException;
use Fitatu\BillingBundle\Exception\TransactionTypeNotProvidedExeption;
use Fitatu\BillingBundle\Factory\PaymentFactory;
use Fitatu\BillingBundle\Factory\PaymentLogFactory;
use Fitatu\BillingBundle\Model\Refund;
use Fitatu\BillingBundle\Provider\BillingProvider;
use Fitatu\BillingBundle\Provider\BillingProviderInterface;
use Fitatu\Cassandra\QueryBuilder;
use Fitatu\DatabaseBundle\Entity\Auth\Payment;
use Fitatu\DatabaseBundle\Entity\Auth\Plan;
use Fitatu\DatabaseBundle\Repository\Auth\AbstractPaymentRepository;
use Fitatu\DatabaseBundle\Repository\Auth\AbstractPlanRepository;
use Fitatu\SharedUserBundle\Exception\UserNotFoundException;
use Fitatu\SharedUserBundle\Exception\UserNotSetException;
use Fitatu\SharedUserBundle\Model\User;
use Fitatu\SharedUserBundle\Repository\UserRepositoryInterface;
use Illuminate\Support\Collection;
use Mockery\Exception;
use Psr\Log\LoggerInterface;
use Symetria\SharedBundle\Event\Subscription\SubscribeUserToPlanEvent;
use Symetria\SharedBundle\EventBus\Dispatcher\RabbitMQEventDispatcherBridge;
use Symetria\SharedBundle\Model\UserInterface;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PaymentService
{
    const LOGS_TABLE_NAME = 'payment_log';

    /**
     * @var QueryBuilder
     */
    private $cassandra;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var Collection
     */
    private $transaction;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $productId;
    /**
     * @var AbstractPlanRepository
     */
    private $planRepository;

    /**
     * @var string
     */
    private $transactionType;

    /**
     * @var AbstractPaymentRepository
     */
    private $paymentRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var UserCacheCleanerService
     */
    private $userCacheCleanerService;

    /**
     * @var RabbitMQEventDispatcherBridge
     */
    private $dispatcher;

    /**
     * @param QueryBuilder                  $cassandra
     * @param SubscriptionService           $subscriptionService
     * @param AbstractPlanRepository        $planRepository
     * @param AbstractPaymentRepository     $paymentRepository
     * @param UserCacheCleanerService       $userCacheCleanerService
     * @param LoggerInterface               $logger
     * @param RabbitMQEventDispatcherBridge $dispatcher
     */
    public function __construct(
        QueryBuilder $cassandra,
        SubscriptionService $subscriptionService,
        AbstractPlanRepository $planRepository,
        AbstractPaymentRepository $paymentRepository,
        UserCacheCleanerService $userCacheCleanerService,
        LoggerInterface $logger,
        RabbitMQEventDispatcherBridge $dispatcher
    ) {
        $this->cassandra = $cassandra;
        $this->subscriptionService = $subscriptionService;
        $this->planRepository = $planRepository;
        $this->paymentRepository = $paymentRepository;
        $this->logger = $logger;
        $this->userCacheCleanerService = $userCacheCleanerService;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param UserRepositoryInterface $userRepository
     */
    public function setUserRepository(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param int|null   $userId
     * @param array|null $transaction
     */
    public function log(int $userId = null, array $transaction = null)
    {
        if ($userId) {
            $this->user = (new User(['id' => $userId]));
        }
        $this->cassandra->table(static::LOGS_TABLE_NAME)->create(
            PaymentLogFactory::create(
                $this->user->getId(),
                $this->transaction
            )
        );
    }

    /**
     * @return bool
     */
    public function validate()
    {
        $this->setUpVariables();
        $this->guard();

        $isValid = $this->getBillingProvider()->validate(
            $this->user->getId(),
            $this->transaction
        );

        $this->logger->info(
            sprintf(
                'Payment Validation (#%s): Transaction: %s | ValidationResponse: %s',
                $this->user->getId(),
                $this->transaction,
                collect($isValid)
            )
        );

        if ($isValid) {
            $this->handleValidPayment();
        }

        return $isValid;
    }

    /**
     * @throws \Exception
     */
    public function handleValidPayment()
    {
        try {
            /** @var Plan $plan */
            $plan = $this->planRepository->findOneBy([
                'providerId' => $this->productId,
                'locale'     => $this->user->getLocale(),
            ]);

            $event = new SubscribeUserToPlanEvent(
                $this->user->getId(),
                $plan->getId(),
                $this->transaction
            );

            $this->dispatcher->dispatch($event);

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param int|UserInterface $user
     * @return PaymentService
     */
    public function setUser($user): PaymentService
    {
        if (is_integer($user)) {
            $user = $this->userRepository->find($user);
        }

        $this->user = $user;

        if (!$this->user instanceof UserInterface) {
            throw new UserNotFoundException($user);
        }

        return $this;
    }

    /**
     * @param string[] $transaction
     * @return PaymentService
     */
    public function setTransaction($transaction): PaymentService
    {
        $this->transaction = collect($transaction)->map(function ($item) {
            if (is_array($item)) {
                return collect($item);
            }

            return $item;
        });

        return $this;
    }

    /**
     * @param string $productId
     * @return PaymentService
     */
    public function setProductId(string $productId): PaymentService
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * @return PaymentService
     */
    private function setUpVariables(): PaymentService
    {
        if ($this->shouldAssignProductId()) {
            $this->setProductId($this->transaction->get('id'));
        }

        if ($this->doesTransactionHaveItsType()) {
            $this->transactionType = $this->transaction->get('transaction')->get('type');
        }

        return $this;
    }

    /**
     * @throws UserNotSetException|TransactionDetailsNotProvidedException|TransactionTypeNotProvidedExeption
     */
    private function guard()
    {
        if (empty($this->user)) {
            throw new UserNotSetException();
        }

        if (empty($this->transaction)) {
            throw new TransactionDetailsNotProvidedException();
        }

        if (empty($this->transactionType)) {
            throw new TransactionTypeNotProvidedExeption();
        }
    }

    /**
     * @return BillingProviderInterface
     */
    protected function getBillingProvider(): BillingProviderInterface
    {
        return BillingProvider::getByTransactionType($this->transactionType);
    }

    /**
     * @param UserInterface $user
     * @param Plan          $plan
     * @return string[]
     */
    protected function getPaymentArray(UserInterface $user, Plan $plan): array
    {
        return PaymentFactory::create(
            $user,
            $plan,
            $this->transaction
        );
    }

    /**
     * @return bool
     */
    private function shouldAssignProductId(): bool
    {
        return empty($this->productId)
               && $this->transaction instanceof Collection
               && $this->transaction->get('id');
    }

    /**
     * @return bool
     */
    private function doesTransactionHaveItsType(): bool
    {
        return $this->transaction instanceof Collection && !empty($this->transaction->get('transaction'));
    }

    /**
     * @param int $paymentId
     * @param int $doerId
     * @throws Exception
     * @return Payment
     */
    public function refund(int $paymentId, int $doerId)
    {
        $payment = $this->paymentRepository->find($paymentId);

        if (!$payment instanceof Payment) {
            throw new PaymentNotFoundException($paymentId);
        }

        try {
            /** @var BillingProviderInterface $billingProvider */
            $billingProvider = BillingProvider::getByTransactionType($payment->getProvider());
            $isRefunded = $billingProvider::refund($payment);
        } catch (\Exception $e) {
            $isRefunded = false;
        }

        if ($isRefunded) {
            $this->paymentRepository->persist(
                $payment = $payment->update([
                    'refundedAt' => new \DateTime(),
                    'refundedBy' => $doerId,
                    'updatedAt'  => new \DateTime(),
                ])
            );
        }

        return new Refund($payment, $isRefunded);
    }
}
