<?php

namespace Fitatu\BillingBundle\Service;

use Fitatu\DatabaseBundle\Entity\Auth\Plan;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PlanFormatter
{
    /**
     * @param Plan   $plan
     * @return array
     */
    public static function format(Plan $plan): array
    {
        /** @var Plan $plan */
        $group = $plan->getPlanGroup();

        return [
            'id'           => $plan->getProviderId(),
            'name'         => $plan->getName(),
            'duration'     => $group->getDuration($inMonths = true),
            'amount'       => PlanAmountFormatter::format($plan, $plan->getAmount()),
            'promoAmount'  => PlanAmountFormatter::format($plan, $plan->getPromoAmount()),
            'isBestseller' => $plan->isBestseller(),
            'message'      => $plan->getMessage(),
            'description'  => $plan->getDescription(),
            'currency'     => $plan->getCurrency(),
            'type'         => $group->getProviderType(),
        ];
    }
}
