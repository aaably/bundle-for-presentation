<?php

namespace Fitatu\BillingBundle\Service;

use Fitatu\DatabaseBundle\Entity\Auth\Plan;
use Fitatu\SharedBundle\Util\Amount;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PlanAmountFormatter
{
    public static function format(Plan $plan, ?int $amount): ?array
    {
        if (!$amount) {
            return null;
        }

        $duration = $plan->getPlanGroup()
                         ->getDuration(true);
        $locale = $plan->getLocale();
        $integer = $amount;
        $monthlyAmount = Amount::parseInteger($amount / $duration)
                               ->setLocale($locale)
                               ->getAsCurrency();

        /** @var Amount $amount */
        $amount = Amount::parseInteger($amount)
                        ->setLocale($locale)
                        ->getAsCurrency();

        return [
            'saving'  => '',
            'monthly' => $monthlyAmount,
            'total'   => $amount,
            'integer' => $integer,
        ];
    }
}
