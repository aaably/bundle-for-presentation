<?php

namespace Fitatu\BillingBundle\Service;

use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Fitatu\BillingBundle\ValueObject\PaymentValueObject;
use Fitatu\BillingBundle\ValueObject\ValueObjectQueryFilter;
use Fitatu\DatabaseBundle\Repository\Auth\AbstractPaymentRepository;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PaymentCollectorService
{
    /**
     * @var AbstractPaymentRepository
     */
    private $paymentRepository;

    /**
     * @param AbstractPaymentRepository $paymentRepository
     */
    public function __construct(AbstractPaymentRepository $paymentRepository)
    {
        $this->paymentRepository = $paymentRepository;
    }

    /**
     * @param PaymentValueObject $filters
     * @param int                $limit
     * @param int                $offset
     * @param string             $orderBy
     * @param string             $order
     *
     * @return Paginator
     */
    public function get(
        PaymentValueObject $filters,
        int $limit = 40,
        int $offset = 0,
        string $orderBy = 'id',
        string $order = 'ASC'
    ): Paginator {
        $query = $this->paymentRepository->createQueryBuilder('p')
                                         ->orderBy('p.'.$orderBy, $order);

        $query = (new ValueObjectQueryFilter($query, $filters, 'p'))->get();

        return $this->paginate($query->getQuery(), $limit, $offset);
    }

    /**
     * @param Query $dql
     * @param int $limit
     * @param int $offset
     *
     * @return Paginator
     */
    public function paginate(Query $dql, int $limit, int $offset): Paginator
    {
        $paginator = new Paginator($dql);

        $paginator->getQuery()
                  ->setFirstResult($offset)
                  ->setMaxResults($limit);

        return $paginator;
    }
}
