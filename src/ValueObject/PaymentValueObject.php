<?php

namespace Fitatu\BillingBundle\ValueObject;

use Symetria\SharedBundle\Model\UserInterface;

/**
 * @author    Nikodem Osmialowski, Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PaymentValueObject implements ValueObjectInterface
{
    /**
    * This field can be used to find a specific payment based on it's ID, providerId or other relevant, searchable fields.
     *
     * @var string|null
     */
    private $phrase;

    /**
     * @var UserInterface|null
     */
    private $user;

    /**
     * @var int|null
     */
    private $planId;

    /**
     * @var int|null
     */
    private $amountFrom;

    /**
     * @var int|null
     */
    private $amountTo;

    /**
     * @var string|null
     */
    private $currency;

    /**
     * @var bool
     */
    private $onlyTest = false;

    /**
     * @var bool
     */
    private $onlyNotTest = false;

    /**
     * @var \DateTimeInterface|null
     */
    private $createdAtFrom;

    /**
     * @var \DateTimeInterface|null
     */
    private $createdAtTo;

    /**
     * @var \DateTimeInterface|null
     */
    private $updatedAtFrom;

    /**
     * @var \DateTimeInterface|null
     */
    private $updatedAtTo;

    /**
     * @var \DateTimeInterface|null
     */
    private $completedAtFrom;

    /**
     * @var \DateTimeInterface|null
     */
    private $completedAtTo;

    /**
     * @param string|null             $phrase
     * @param UserInterface|null      $user
     * @param int|null                $planId
     * @param int|null                $amountFrom
     * @param int|null                $amountTo
     * @param string|null             $currency
     * @param bool                    $onlyTest
     * @param bool                    $onlyNotTest
     * @param \DateTimeInterface|null $createdAtFrom
     * @param \DateTimeInterface|null $createdAtTo
     * @param \DateTimeInterface|null $updatedAtFrom
     * @param \DateTimeInterface|null $updatedAtTo
     * @param \DateTimeInterface|null $completedAtFrom
     * @param \DateTimeInterface|null $completedAtTo
     */
    public function __construct(
        ?string $phrase,
        ?UserInterface $user,
        ?int $planId,
        ?int $amountFrom,
        ?int $amountTo,
        ?string $currency,
        bool $onlyTest,
        bool $onlyNotTest,
        ?\DateTimeInterface $createdAtFrom,
        ?\DateTimeInterface $createdAtTo,
        ?\DateTimeInterface $updatedAtFrom,
        ?\DateTimeInterface $updatedAtTo,
        ?\DateTimeInterface $completedAtFrom,
        ?\DateTimeInterface $completedAtTo
    ) {
        $this->phrase = $phrase;
        $this->user = $user;
        $this->planId = $planId;
        $this->amountFrom = $amountFrom;
        $this->amountTo = $amountTo;
        $this->currency = $currency;
        $this->onlyTest = $onlyTest;
        $this->onlyNotTest = $onlyNotTest;
        $this->createdAtFrom = $createdAtFrom;
        $this->createdAtTo = $createdAtTo;
        $this->updatedAtFrom = $updatedAtFrom;
        $this->updatedAtTo = $updatedAtTo;
        $this->completedAtFrom = $completedAtFrom;
        $this->completedAtTo = $completedAtTo;
    }

    /**
     * @return PaymentValueObject
     */
    public static function createEmpty(): PaymentValueObject
    {
        return static::createFromArray([]);
    }

    /**
     * @param array $values
     *
     * @return PaymentValueObject
     */
    public static function createFromArray(array $values): PaymentValueObject
    {
        $values = collect($values);

        return new static(
            static::castNullableString($values->get('phrase')),
            $values->get('user') instanceof UserInterface ? $values->get('user') : null,
            static::castNullableInteger($values->get('plan')),
            static::castNullableInteger($values->get('amountFrom')),
            static::castNullableInteger($values->get('amountTo')),
            static::castNullableString($values->get('currency')),
            static::castBoolean($values->get('onlyTest')),
            static::castBoolean($values->get('onlyNotTest')),
            static::castNullableDateTime($values->get('createdAtFrom'), 'low'),
            static::castNullableDateTime($values->get('createdAtTo'), 'high'),
            static::castNullableDateTime($values->get('updatedAtFrom'), 'low'),
            static::castNullableDateTime($values->get('updatedAtTo'), 'high'),
            static::castNullableDateTime($values->get('completedAtFrom'), 'low'),
            static::castNullableDateTime($values->get('completedAtTo'), 'high')
        );
    }

    /**
     * @param mixed $value
     *
     * @return null|string
     */
    private static function castNullableString($value): ?string
    {
        return empty($value) ? null : (string)$value;
    }

    /**
     * @param mixed $value
     *
     * @return int|null
     */
    private static function castNullableInteger($value): ?int
    {
        return empty($value) ? null : (int)$value;
    }

    /**
     * @param mixed $value
     *
     * @return bool
     */
    private static function castBoolean($value): bool
    {
        return empty($value) ? false : (bool)$value;
    }

    /**
     * @param mixed       $value
     * @param string|null $highLowTime
     *
     * @return \DateTime|null
     */
    private static function castNullableDateTime($value, ?string $highLowTime): ?\DateTime
    {
        $value = empty($value) ? null : new \DateTime($value);

        if (!$value instanceof \DateTime) {
            return null;
        }

        if ($highLowTime == 'high') {
            $value->setTime(23, 59, 59);
        } elseif ($highLowTime == 'low') {
            $value->setTime(0, 0, 0);
        }

        return $value;
    }

    /**
     * @return string|null
     */
    public function getPhrase(): ?string
    {
        return $this->phrase;
    }

    /**
     * @return UserInterface|null
     */
    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    /**
     * @return int|null
     */
    public function getPlanId(): ?int
    {
        return $this->planId;
    }

    /**
     * @return int|null
     */
    public function getAmountFrom(): ?int
    {
        return $this->amountFrom;
    }

    /**
     * @return int|null
     */
    public function getAmountTo(): ?int
    {
        return $this->amountTo;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @return bool
     */
    public function isOnlyTest(): bool
    {
        return $this->onlyTest;
    }

    /**
     * @return bool
     */
    public function isOnlyNotTest(): bool
    {
        return $this->onlyNotTest;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedAtFrom(): ?\DateTimeInterface
    {
        return $this->createdAtFrom;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedAtTo(): ?\DateTimeInterface
    {
        return $this->createdAtTo;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAtFrom(): ?\DateTimeInterface
    {
        return $this->updatedAtFrom;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAtTo(): ?\DateTimeInterface
    {
        return $this->updatedAtTo;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCompletedAtFrom(): ?\DateTimeInterface
    {
        return $this->completedAtFrom;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCompletedAtTo(): ?\DateTimeInterface
    {
        return $this->completedAtTo;
    }
}
