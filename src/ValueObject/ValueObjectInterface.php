<?php

namespace Fitatu\BillingBundle\ValueObject;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
interface ValueObjectInterface
{
    /**
     * @param array $fields
     *
     * @return ValueObjectInterface
     */
    public static function createFromArray(array $fields);

    /**
     * @return string|null
     */
    public function getPhrase(): ?string;
}