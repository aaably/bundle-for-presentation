<?php

namespace Fitatu\BillingBundle\ValueObject;

use Doctrine\ORM\QueryBuilder;
use Fitatu\SharedBundle\Util\Amount;
use Symetria\SharedBundle\Model\UserInterface;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class ValueObjectQueryFilter
{
    /**
     * @var QueryBuilder
     */
    private $query;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var ValueObjectInterface
     */
    private $valueObject;

    /**
     * @param QueryBuilder         $query
     * @param ValueObjectInterface $valueObject
     * @param string               $alias
     */
    public function __construct(QueryBuilder $query, ValueObjectInterface $valueObject, string $alias)
    {
        $this->query = $query;
        $this->alias = $alias;
        $this->valueObject = $valueObject;
    }

    /**
     * @param $phrase
     */
    public function getPhraseFilter($phrase)
    {
        $exp = $this->getQueryBuilder()->expr();
        $this->query = $this->query->where(
            $exp->like($this->alias.'.id', $exp->literal('%'.$phrase.'%'))
        )->orWhere(
            $exp->like($this->alias.'.providerId', $exp->literal('%'.$phrase.'%'))
        )->orWhere($this->alias.'.user = :user')->setParameter('user', $phrase);
    }

    /**
     * @param UserInterface $user
     */
    public function getUserFilter(UserInterface $user)
    {
        $this->query = $this->query
            ->andWhere($this->alias.'.user = :user')
            ->setParameter('user', $user);
    }

    /**
     * @param int|null $planId
     */
    public function getPlanIdFilter(?int $planId)
    {
        $this->query = $this->query
            ->andWhere($this->alias.'.plan = :planId')
            ->setParameter('planId', $planId);
    }

    /**
     * @param int|string|null $amount
     */
    public function getAmountFromFilter($amount)
    {
        $amount = (new Amount($amount))->getAsInteger();

        $this->query = $this->query
            ->andWhere($this->alias.'.amount >= :amountFrom')
            ->setParameter('amountFrom', $amount);
    }

    /**
     * @param int|string|null $amount
     */
    public function getAmountToFilter($amount)
    {
        $amount = (new Amount($amount))->getAsInteger();

        $this->query = $this->query
            ->andWhere($this->alias.'.amount <= :amountTo')
            ->setParameter('amountTo', $amount);
    }

    /**
     * @param null|string $currency
     */
    public function getCurrencyFilter(?string $currency)
    {
        $this->query = $this->query
            ->andWhere($this->alias.'.currency = :currency')
            ->setParameter('currency', $currency);
    }

    /**
     * @param bool $isOnlyTest
     */
    public function isOnlyTestFilter(bool $isOnlyTest)
    {
        if (!$isOnlyTest) {
            return;
        }

        $this->query = $this->query
            ->andWhere($this->alias.'.test = 1');
    }

    /**
     * @param bool $isOnlyNotTest
     */
    public function isOnlyNotTestFilter(bool $isOnlyNotTest)
    {
        if (!$isOnlyNotTest) {
            return;
        }
        $this->query = $this->query
            ->andWhere($this->alias.'.test = 0');
    }

    /**
     * @param $date
     */
    public function getCreatedAtFromFilter($date)
    {
        $this->query = $this->query
            ->andWhere($this->alias.'.createdAt >= :createdAtFrom')
            ->setParameter('createdAtFrom', $date);
    }

    /**
     * @param $date
     */
    public function getCreatedAtToFilter($date)
    {
        $this->query = $this->query
            ->andWhere($this->alias.'.createdAt <= :createdAtTo')
            ->setParameter('createdAtTo', $date);
    }

    /**
     * @param $date
     */
    public function getUpdatedAtFromFilter($date)
    {
        $this->query = $this->query
            ->andWhere($this->alias.'.updatedAt >= :updatedAtFrom')
            ->setParameter('updatedAtFrom', $date);
    }

    /**
     * @param $date
     */
    public function getUpdatedAtToFilter($date)
    {
        $this->query = $this->query
            ->andWhere($this->alias.'.updatedAt <= :updatedAtTo')
            ->setParameter('updatedAtTo', $date);
    }

    /**
     * @param $date
     */
    public function getCompletedAtFromFilter($date)
    {
        $this->query = $this->query
            ->andWhere($this->alias.'.completedAt >= :completedAtFrom')
            ->setParameter('completedAtFrom', $date);
    }

    /**
     * @param $date
     */
    public function getCompletedAtToFilter($date)
    {
        $this->query = $this->query
            ->andWhere($this->alias.'.completedAt <= :completedAtTo')
            ->setParameter('completedAtTo', $date);
    }

    /**
     * @return ValueObjectQueryFilter
     */
    public function filter(): ValueObjectQueryFilter
    {
        $methods = get_class_methods($this->valueObject);

        foreach ($methods as $method) {
            $methodName = $method;
            $filterMethodName = $methodName.'Filter';

            if (method_exists($this, $filterMethodName) && !is_null($this->valueObject->{$methodName}())) {
                $this->{$filterMethodName}(
                    $this->valueObject->{$methodName}()
                );
            }
        }



        return $this;
    }

    /**
     * @return QueryBuilder
     */
    public function getQueryBuilder()
    {
        return $this->query;
    }

    /**
     * @return QueryBuilder
     */
    public function get(): QueryBuilder
    {
        return $this->filter()->getQueryBuilder();
    }
}
