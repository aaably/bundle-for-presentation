<?php

namespace Fitatu\BillingBundle\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class EmptyIOSReceiptException extends HttpException
{
    public function __construct()
    {
        parent::__construct(422, "Transaction receipt can not be empty.");
    }
}
