<?php

namespace Fitatu\BillingBundle\Exception;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PaymentNotFoundException extends \Exception
{
    const MESSAGE = 'Payment not found (%s)';

    /**
     * @param int $paymentId
     */
    public function __construct(int $paymentId)
    {
        parent::__construct(
            sprintf(
                static::MESSAGE,
                $paymentId
            )
        );
    }
}