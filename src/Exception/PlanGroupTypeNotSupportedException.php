<?php

namespace Fitatu\BillingBundle\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PlanGroupTypeNotSupportedException extends HttpException
{
    const MESSAGE = "Plan Group Type (%s) not found";

    /**
     * @param string $type
     */
    public function __construct(string $type)
    {
        parent::__construct(
            Response::HTTP_NOT_FOUND,
            sprintf(static::MESSAGE, $type)
        );
    }
}
