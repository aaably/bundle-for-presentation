<?php

namespace Fitatu\BillingBundle\Exception;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class SubscriptionPlanGroupNotFoundException extends AbstractNotFoundException
{
    const MESSAGE = 'Subscription Plan Group %d not found.';
}