<?php

namespace Fitatu\BillingBundle\Exception;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class TransactionDetailsNotProvidedException extends \LogicException
{
    const MESSAGE = 'Transaction details not provided.';

    public function __construct()
    {
        parent::__construct(static::MESSAGE);
    }
}
