<?php

namespace Fitatu\BillingBundle\Exception;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class SubscriptionPlanNotFoundException extends AbstractNotFoundException
{
    const MESSAGE = 'Subscription Plan %d not found.';
}