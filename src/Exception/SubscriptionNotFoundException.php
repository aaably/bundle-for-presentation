<?php

namespace Fitatu\BillingBundle\Exception;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class SubscriptionNotFoundException extends AbstractNotFoundException
{
    const MESSAGE = 'Subscription with given id (#%d) was not found';
}