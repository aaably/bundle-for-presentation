<?php

namespace Fitatu\BillingBundle\Exception;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class CancelNotSupportedException extends \Exception
{
    const MESSAGE = 'This billing provider (%s) does not support subscription cancel';

    /**
     * @param string $providerName
     */
    public function __construct(string $providerName)
    {
        parent::__construct(
            sprintf(
                static::MESSAGE,
                $providerName
            )
        );
    }

}