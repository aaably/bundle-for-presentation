<?php

namespace Fitatu\BillingBundle\Exception;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class UserAlreadyHasActiveSubscribtionException extends \LogicException
{
    public function __construct(int $userId)
    {
        parent::__construct(
            sprintf("User #%d already has an active subscribtion", $userId)
        );
    }
}
