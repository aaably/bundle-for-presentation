<?php

namespace Fitatu\BillingBundle\Exception;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class UserNotFoundException extends AbstractNotFoundException
{
    const MESSAGE = 'User was not found (#%d)';
}