<?php

namespace Fitatu\BillingBundle\Exception;

use LogicException;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
abstract class AbstractNotFoundException extends LogicException
{
    /**
     * @param int|string     $Id
     * @param int            $code
     * @param Exception|null $previous
     */
    public function __construct($Id, int $code = 0, Exception $previous = null)
    {
        $code = $code ?: Response::HTTP_UNPROCESSABLE_ENTITY;

        $msg = sprintf(static::MESSAGE, $Id);
        parent::__construct(
            $msg,
            $code,
            $previous
        );
    }
}