<?php

namespace Fitatu\BillingBundle\Exception;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class RefundException extends Exception
{
    const MESSAGE = "Refund for payment %s failed.";

    /**
     * @param string $paymentId
     */
    public function __construct(string $paymentId)
    {
        parent::__construct(
            sprintf(static::MESSAGE, $paymentId),
            Response::HTTP_BAD_REQUEST
        );
    }
}