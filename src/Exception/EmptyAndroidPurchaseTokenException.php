<?php

namespace Fitatu\BillingBundle\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class EmptyAndroidPurchaseTokenException extends HttpException
{
    public function __construct()
    {
        parent::__construct(422, 'Andoid purchase token was not provided');
    }
}
