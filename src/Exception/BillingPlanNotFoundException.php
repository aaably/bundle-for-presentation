<?php

namespace Fitatu\BillingBundle\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;
/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class BillingPlanNotFoundException extends HttpException
{
    public function __construct()
    {
        parent::__construct(
            404,
            "Billing Plan was not found."
        );
    }
}