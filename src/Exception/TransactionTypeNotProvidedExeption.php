<?php

namespace Fitatu\BillingBundle\Exception;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class TransactionTypeNotProvidedExeption extends \LogicException
{
    const MESSAGE = 'Transaction type not provided.';

    public function __construct()
    {
        parent::__construct(static::MESSAGE);
    }
}
