FitatuBillingBundle
==================

Spis treści
-----------

1. [Narzędzia](#narzędzia)
2. [Odpalanie testów](#odpalanie-testów)

Narzędzia
---------

* [Jenkins (Continuous Integration)](http://217.168.143.190:9999/job/Fitatu%20Billing%20Bundle/)

Odpalanie testów
----------------

```bash
make spec-test
make unit-test
make functional-test
```

Opcjonalnie, ze zrzucaniem raportów w formatach HTML i Clover:

```bash
make unit-report
make functional-report
```

I raporty + format --tap:

```bash
make unit-report-tap
make functional-report-tap
```