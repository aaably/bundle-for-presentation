<?php

namespace Tests\BillingBundle\Unit\Service;

use Fitatu\BillingBundle\Service\PlanService;
use Fitatu\DatabaseBundle\Entity\Auth\Plan;
use Fitatu\DatabaseBundle\Entity\Auth\PlanGroup;
use Fitatu\DatabaseBundle\Repository\Auth\AbstractPlanRepository;
use Illuminate\Support\Collection;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 * @group     unit
 */
class PlanServiceTest extends \PHPUnit_Framework_TestCase
{
    const MONTHLY_PRICE = 20;
    const YEARLY_PRICE = 120;
    // 20 * 12 - 120 = 120
    const SHOULD_SAVE = '120,00 zł';
    /**
     * @var Plan
     */
    private $monthly;
    /**
     * @var Plan
     */
    private $yearly;
    /**
     * @var PlanGroupService
     */
    private $service;

    public function createGroupWithPlans()
    {
        $yearlyGroup = new PlanGroup('Yearly');
        $yearlyGroup->setDuration(360);

        $monthlyGroup = new PlanGroup('Monthly');
        $monthlyGroup->setDuration(30);

        $this->yearly = new Plan($yearlyGroup);
        $this->yearly->setAmount(static::YEARLY_PRICE);
        $this->yearly->setCurrency('PLN')->setLocale('pl_PL');

        $this->monthly = new Plan($monthlyGroup);
        $this->monthly->setAmount(static::MONTHLY_PRICE);
        $this->monthly->setCurrency('PLN')->setLocale('pl_PL');

    }

    /**
     * @param array $plan
     * @return float
     */
    private function getTotalAmountFromItem(array $plan): float
    {
        return floatval($plan['amount']['total']);
    }

    /**
     * @param array $plan
     * @return float
     */
    private function getMonthlyAmountFromItem(array $plan): float
    {
        return floatval($plan['amount']['monthly']);
    }

    private function createService()
    {
        /** @var AbstractPlanRepository $repository */
        $repository = $this->getMockBuilder(AbstractPlanRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->service = new PlanService($repository);
    }

    public function setUp()
    {
        parent::setUp();

        $this->createGroupWithPlans();
        $this->createService();
    }

    /**
     * @test
     */
    public function it_collects_and_formats_data()
    {
        $collection = $this->service->collectData([
            $this->yearly, $this->monthly
        ]);

        $this->assertTrue($collection instanceof Collection);
        $this->assertCount(2, $collection->toArray());

        return $this->service->transformPlansCollection($collection);
    }

    /**
     * @test
     * @depends it_collects_and_formats_data
     * @param Collection $collection
     */
    public function it_transforms_collection_data(Collection $collection)
    {
        $this->assertTrue($collection instanceof Collection);

        $first = $collection->first();
        $last = $collection->last();

        $this->assertTrue($this->getTotalAmountFromItem($first) > $this->getTotalAmountFromItem($last));
        $this->assertTrue($this->getMonthlyAmountFromItem($first) < $this->getMonthlyAmountFromItem($last));

        $this->assertEquals(static::SHOULD_SAVE, $first['amount']['saving']);
        $this->assertEquals("", $last['amount']['saving']);

        $this->assertEquals(floatval(static::SHOULD_SAVE), floatval($this->service->calculateSavings($first, $last)));
    }
}
