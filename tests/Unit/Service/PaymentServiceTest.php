<?php

namespace Tests\BillingBundle\Unit\Service;

use Fitatu\ApiClientBundle\Service\UserCacheCleanerService;
use Fitatu\BillingBundle\DataFixtures\ORM\PlanGroupFixtures;
use Fitatu\BillingBundle\Factory\PaymentLogFactory;
use Fitatu\BillingBundle\Provider\AndroidBillingProvider;
use Fitatu\BillingBundle\Provider\BillingProviderInterface;
use Fitatu\BillingBundle\Service\PaymentService;
use Fitatu\BillingBundle\Service\SubscriptionService;
use Fitatu\Cassandra\QueryBuilder;
use Fitatu\DatabaseBundle\Entity\Auth\Plan;
use Fitatu\DatabaseBundle\Entity\Auth\PlanGroup;
use Fitatu\DatabaseBundle\Entity\Auth\Subscription;
use Fitatu\DatabaseBundle\Repository\Auth\AbstractPaymentRepository;
use Fitatu\DatabaseBundle\Repository\Auth\AbstractPlanRepository;
use Fitatu\SharedUserBundle\Repository\UserRepositoryInterface;
use Illuminate\Support\Collection;
use Mockery;
use Mockery\MockInterface;
use Psr\Log\LoggerInterface;
use Symetria\SharedBundle\EventBus\Dispatcher\RabbitMQEventDispatcherBridge;
use Symetria\SharedBundle\Mapper\UserSystemMapper;
use Symetria\SharedBundle\Model\UserInterface as User;
use Tests\BillingBundle\Fake\PaymentServiceFake;
use Tests\BillingBundle\Fake\UserFake;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PaymentServiceTest extends \PHPUnit_Framework_TestCase
{
    const USER_ID = 1;
    const USER_LOCALE = 'pl_PL';
    const PROVIDER = UserSystemMapper::ANDROID_ID;
    const VALID_PAYMENT_PAYLOAD = [
        'id'          => 'uniqueProviderId',
        'transaction' => [
            'type' => 'android-playstore',
        ],
        'price'       => 12300000, /// in micros
        'vaid'        => true,
    ];
    const PLAN_DURATION = PlanGroupFixtures::PLAN_DURATION;
    const INVALID_PAYMENT_PAYLOAD = [
        'id'          => 'sss',
        'transaction' => [
            'type' => 'android-playstore',
        ],
        'vaid'        => true,
    ];

    /**
     * @var QueryBuilder|MockInterface
     */
    protected $cassandra;

    /**
     * @var UserRepository|MockInterface
     */
    protected $userRepository;

    /**
     * @var SubscriptionService|MockInterface
     */
    protected $subscriptionService;

    /**
     * @var PaymentServiceFake
     */
    protected $paymentService;

    /**
     * @var AbstractPlanRepository|MockInterface
     */
    protected $planRepository;

    /**
     * @var AbstractPaymentRepository|MockInterface
     */
    protected $paymentRepository;

    /**
     * @var Collection
     */
    private $transaction;

    /**
     * @var LoggerInterface|MockInterface
     */
    private $logger;

    /**
     * @var UserCacheCleanerService|MockInterface
     */
    private $userCacheCleanerService;

    /**
     * @var RabbitMQEventDispatcherBridge
     */
    private $dispatcher;

    public function setUp()
    {
        parent::setUp();
        $this->createService();

        $this->transaction = collect(static::VALID_PAYMENT_PAYLOAD)
            ->map(function ($item) {
                if (is_array($item)) {
                    return collect($item);
                }

                return $item;
            });
    }

    public function createService()
    {
        $this->cassandra = $this->mock(QueryBuilder::class);
        $this->subscriptionService = $this->mock(SubscriptionService::class);
        $this->paymentRepository = $this->mock(AbstractPaymentRepository::class);
        $this->planRepository = $this->mock(AbstractPlanRepository::class);
        $this->userRepository = $this->mock(UserRepositoryInterface::class);
        $this->userCacheCleanerService = $this->mock(UserCacheCleanerService::class);
        $this->logger = $this->mock(LoggerInterface::class);
        $this->dispatcher = $this->mock(RabbitMQEventDispatcherBridge::class );

        $this->paymentService = new PaymentServiceFake(
            $this->cassandra,
            $this->subscriptionService,
            $this->planRepository,
            $this->paymentRepository,
            $this->userCacheCleanerService,
            $this->logger,
            $this->dispatcher
        );
        $this->paymentService->setUserRepository(
            $this->userRepository
        );
    }

    /**
     * @test
     */
    public function it_logs_transactions()
    {
        $this->userRepository
            ->shouldReceive('find')
            ->with(1)
            ->andReturn($this->getUser());

        // if is valid it finds selected plan
        $planGroup = Mockery::mock(PlanGroup::class);
        $plan = Mockery::mock(Plan::class);
        $this->planRepository
            ->shouldReceive('findOneBy')
            ->with([
                'provider_id' => static::PROVIDER,
                'locale'      => static::USER_LOCALE,
            ])->andReturn($plan);

        $this->cassandra
            ->shouldReceive('table')
            ->with(PaymentService::LOGS_TABLE_NAME)
            ->andReturn($this->cassandra);

        $this->cassandra
            ->shouldReceive('create')
            ->with(PaymentLogFactory::create(
                static::USER_ID,
                $this->transaction
            ))->andReturn('test');

        $this->paymentService
            ->setUser(static::USER_ID)
            ->setTransaction(static::VALID_PAYMENT_PAYLOAD)
            ->log();
    }

    /**
     * @test
     */
    public function it_validates_transaction()
    {
        /** @var BillingProviderInterface $billingProvider */
        $billingProvider = Mockery::mock(AndroidBillingProvider::class);
        $this->paymentService->setBillingProvider($billingProvider);
        // it gets user
        $this->userRepository
            ->shouldReceive('find')
            ->with(1)
            ->andReturn($this->getUser());

        // then validation method is triggered
        $billingProvider->shouldReceive('validate')
            ->andReturn(true);

        // we logg this transaction
        $this->logger->shouldReceive('log')
            ->with('Payment Validation')
            ->andReturn('true');

        $plan = Mockery::spy(Plan::class)
            ->makePartial()
            ->shouldReceive([
                'getId' => 1
            ])->getMock();

        $this->planRepository
            ->shouldReceive('findOneBy')
            ->andReturn($plan);

        // then subscription is beeing created
        $subscription = Mockery::mock(Subscription::class);

        $this->subscriptionService
            ->shouldReceive('subscribeUserToPlan')
            ->andReturn($subscription);

        // subscription should keep user's information
        $subscription->shouldReceive('getUser')
            ->andReturn($this->getUser());

        // then we record success payment
        $this->paymentRepository
            ->shouldReceive('create');

        $this->paymentService
            ->setUser($this->getUser())
            ->setTransaction(static::VALID_PAYMENT_PAYLOAD)
            ->validate();
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return UserFake::make();
    }

    /**
     * @param string $class
     * @return MockInterface
     */
    public function mock(string $class): MockInterface
    {
        return Mockery::spy($class);
    }
}
