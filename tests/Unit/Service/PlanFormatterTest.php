<?php

namespace Tests\BillingBundle\Unit\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Fitatu\BillingBundle\Service\PlanGroupFormatter;
use Fitatu\DatabaseBundle\Entity\Auth\Plan;
use Fitatu\DatabaseBundle\Entity\Auth\PlanGroup;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 * @group     unit
 */
class PlanFormatterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function it_formats_output_data()
    {
        $group = (new PlanGroup('Nowa grupa'))
            ->setDuration(30);

        $plan = (new Plan($group))
            ->setAmount(1200)
            ->setCurrency('PLN')
            ->setLocale('pl_PL');

        $group->setPlans(new ArrayCollection([$plan]));

        $formatted = PlanGroupFormatter::format($group, 'pl_PL');

        $this->assertTrue(is_array($formatted));
        $this->assertArrayHasKey('name', $formatted);
        $this->assertArrayHasKey('duration', $formatted);
        $this->assertArrayHasKey('amount', $formatted);
        $this->assertArrayHasKey('currency', $formatted);
    }

}
