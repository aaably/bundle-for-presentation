<?php

namespace Tests\BillingBundle\Unit\Service;

use DateTime;
use Fitatu\BillingBundle\Service\SubscriptionService;
use Fitatu\DatabaseBundle\Repository\Auth\AbstractPlanRepository;
use Fitatu\DatabaseBundle\Repository\Auth\AbstractSubscriptionRepository;
use Fitatu\Local\Repository\Auth\PlanRepository;
use Fitatu\Local\Repository\Auth\RoleRepository;
use Fitatu\Local\Repository\Auth\Settings\DietGeneratorRepository;
use Fitatu\Local\Repository\Auth\SubscriptionRepository;
use Fitatu\Local\Repository\Auth\User\UserRepository;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 * @group     unit
 */
class SubscriptionServiceTest extends \PHPUnit_Framework_TestCase
{
    const PLAN_GROUP_NAME = 'Nowy plan';

    const LOCALE = 'pl_PL';

    /**
     * @var SubscriptionService
     */
    private $service;

    /**
     * @var AbstractSubscriptionRepository|PHPUnit_Framework_MockObject_MockObject
     */
    private $subscriptionRepository;

    /**
     * @var AbstractPlanRepository|PHPUnit_Framework_MockObject_MockObject
     */
    private $planRepository;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        parent::setUp();
        $this->createService();
    }

    private function createService()
    {
        $this->subscriptionRepository = $this->getMockBuilder(AbstractSubscriptionRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->planRepository = $this->getMockBuilder(AbstractPlanRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->service = new SubscriptionService(
            $this->subscriptionRepository,
            $this->planRepository
        );
    }

    /**
     * @test
     */
    public function it_creates_a_date_scope()
    {
        $days = 30;
        list($from, $to) = $this->service->createDatesScope(null, null, $days);

        $this->assertEquals(
            (new DateTime())->format('d-m-Y'),
            $from->format('d-m-Y')
        );
        $interval = $from->diff($to);

        $this->assertEquals($days, $interval->days);
    }
}
