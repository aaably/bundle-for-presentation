<?php

namespace Tests\Unit\ValueObject;

use Fitatu\BillingBundle\ValueObject\PaymentValueObject;
use Fitatu\SharedUserBundle\Model\User;

/**
 * @author    Nikodem Osmialowski
 * @copyright Fitatu Sp. z o.o.
 */
class PaymentValueObjectTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @small
     */
    public function it_can_be_created_with_empty_values()
    {
        $sut = PaymentValueObject::createEmpty();

        $this->assertNull($sut->getPhrase());
        $this->assertNull($sut->getUser());
        $this->assertNull($sut->getPlanId());
        $this->assertNull($sut->getAmountFrom());
        $this->assertNull($sut->getAmountTo());
        $this->assertNull($sut->getCurrency());
        $this->assertFalse($sut->isOnlyTest());
        $this->assertFalse($sut->isOnlyNotTest());
        $this->assertNull($sut->getCreatedAtFrom());
        $this->assertNull($sut->getCreatedAtTo());
        $this->assertNull($sut->getUpdatedAtFrom());
        $this->assertNull($sut->getUpdatedAtTo());
        $this->assertNull($sut->getCompletedAtFrom());
        $this->assertNull($sut->getCompletedAtTo());
    }

    /**
     * @test
     * @small
     */
    public function it_can_be_created_through_a_static_constructor()
    {
        $values = [
            'phrase'          => 'Foo',
            'user'            => new User(['id' => 1]),
            'plan'            => 2,
            'amountFrom'      => 3,
            'amountTo'        => 4,
            'currency'        => 'JPY',
            'onlyTest'        => true,
            'onlyNotTest'     => true,
            'createdAtFrom'   => '2017-12-11 08:00:00',
            'createdAtTo'     => '2017-12-12 09:00:00',
            'updatedAtFrom'   => '2017-12-13 10:00:00',
            'updatedAtTo'     => '2017-12-14 11:00:00',
            'completedAtFrom' => '2017-12-15 12:00:00',
            'completedAtTo'   => '2017-12-16 13:00:00',
        ];

        $sut = PaymentValueObject::createFromArray($values);

        $this->assertEquals($values['phrase'], $sut->getPhrase());
        $this->assertInstanceOf(User::class, $sut->getUser());
        $this->assertEquals(1, $sut->getUser()->getId());
        $this->assertEquals($values['plan'], $sut->getPlanId());
        $this->assertEquals($values['amountFrom'], $sut->getAmountFrom());
        $this->assertEquals($values['amountTo'], $sut->getAmountTo());
        $this->assertEquals($values['currency'], $sut->getCurrency());
        $this->assertTrue($sut->isOnlyTest());
        $this->assertTrue($sut->isOnlyNotTest());
        $this->assertEquals('2017-12-11 00:00:00', $sut->getCreatedAtFrom()->format('Y-m-d H:i:s'));
        $this->assertEquals('2017-12-12 23:59:59', $sut->getCreatedAtTo()->format('Y-m-d H:i:s'));
        $this->assertEquals('2017-12-13 00:00:00', $sut->getUpdatedAtFrom()->format('Y-m-d H:i:s'));
        $this->assertEquals('2017-12-14 23:59:59', $sut->getUpdatedAtTo()->format('Y-m-d H:i:s'));
        $this->assertEquals('2017-12-15 00:00:00', $sut->getCompletedAtFrom()->format('Y-m-d H:i:s'));
        $this->assertEquals('2017-12-16 23:59:59', $sut->getCompletedAtTo()->format('Y-m-d H:i:s'));
    }
}
