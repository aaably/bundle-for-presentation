<?php
namespace Tests\BillingBundle\Unit\Model;

use Fitatu\BillingBundle\Model\IosNotification;
use Tests\BillingBundle\Fake\IosNotificationDataFake;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class IosNotificationTest extends \PHPUnit_Framework_TestCase
{
    /**
    * @test 
    */
    public function it_sets_values_from_response()
    {
        $notification = IosNotification::create(
            IosNotificationDataFake::getData()
        );

        $this->assertInstanceOf(\DateTime::class, $notification->getPurchaseDate());
        $this->assertInstanceOf(\DateTime::class, $notification->getExpiresDate());
        $this->assertInstanceOf(\DateTime::class, $notification->getPurchaseDate());

        $this->assertInternalType("boolean", $notification->getAutoRenewStatus());
        $this->assertInternalType("boolean", $notification->isInIntroOfferPeriod());
        $this->assertInternalType("boolean", $notification->isTrialPeriod());

        $this->assertInternalType("integer", $notification->getQuantity());
        $this->assertInternalType("integer", $notification->getAppItemId());
        $this->assertInternalType("integer", $notification->getWebOrderLineItemId());
        $this->assertInternalType("integer", $notification->getOriginalPurchaseDateMs());
        $this->assertInternalType("integer", $notification->getPurchaseDateMs());
    }
}