<?php

namespace Tests\BillingBundle;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\BillingBundle\Fake\Config\AppKernel;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class BundleTestCase extends WebTestCase
{
    private $container;

    public function setUp()
    {
        parent::setUp();

        $kernel = new AppKernel('test', true);
        $kernel->boot();
        $this->container = $kernel->getContainer();
    }
}