<?php

namespace Tests\BillingBundle\Functional\Service;

use DateTime;
use Fitatu\BillingBundle\DataFixtures\ORM\PlanFixtures;
use Fitatu\BillingBundle\DataFixtures\ORM\PlanGroupFixtures;
use Fitatu\BillingBundle\Service\SubscriptionService;
use Fitatu\DatabaseBundle\Entity\Auth\Subscription;
use Fitatu\DatabaseBundle\Repository\Auth\AbstractSubscriptionRepository;
use Fitatu\DatabaseBundle\Repository\Auth\Settings\AbstractDietGeneratorRepository;
use Fitatu\DatabaseBundle\Repository\Auth\User\AbstractUserRepository;
use Symetria\ApiServerBundle\Tests\AbstractApiWebTestCase;
use Symetria\UserBundle\DataFixtures\ORM\User\RoleFixtures;
use Symetria\UserBundle\DataFixtures\ORM\User\UserFixtures;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class SubscriptionServiceTest extends AbstractApiWebTestCase
{
    const USER_ID = 1;
    const PLAN_ID = 1;
    const PLAN_DURATION = PlanGroupFixtures::PLAN_DURATION;

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var AbstractSubscriptionRepository
     */
    private $subscriptionRepository;

    /**
     * @var AbstractUserRepository
     */
    private $userRepository;

    /**
     * @var AbstractDietGeneratorRepository
     */
    private $dietGeneratorRepository;

    public function setUp()
    {
        parent::setUp();

        $this->subscriptionService = $this->getContainer()->get('fitatu.billing.subscription_service');
        $this->subscriptionRepository = $this->getContainer()->get('fitatu.billing.repository.subscription');
        $this->userRepository = $this->getContainer()->get(AbstractUserRepository::class);
        $this->dietGeneratorRepository = $this->getContainer()->get(AbstractDietGeneratorRepository::class);
    }

    /**
     * @test
     */
    public function it_creates_user_subscription_and_adds_premium_role()
    {
        $this->loadFixtures([
            RoleFixtures::class,
            UserFixtures::class,
            PlanGroupFixtures::class,
            PlanFixtures::class,
        ]);

        $this->subscriptionService->subscribeUserToPlan(static::USER_ID, static::PLAN_ID);

        /** @var Subscription $subscription */
        $subscription = $this->subscriptionRepository->findOneBy([
            'user' => static::USER_ID,
            'plan' => static::PLAN_ID
        ]);
        $this->assertEquals(
            (new DateTime())->format('d-m-Y'), // subsription starts today
            $subscription->getStartDate()->format('d-m-Y')
        );
        $interval = $subscription->getStartDate()->diff(
            $subscription->getEndDate()
        );
        $this->assertEquals(static::PLAN_DURATION, $interval->days);
        $this->assertTrue($this->subscriptionService->isActive(static::USER_ID));
        $user = $this->userRepository->find(static::USER_ID);
        $this->assertTrue(
            $user->isPremium()
        );
        $dietGenerator= $this->dietGeneratorRepository->findOneBy([
            'user' => static::USER_ID
        ]);
        $this->assertFalse(empty($dietGenerator));
        $this->assertFalse($dietGenerator->isReviewed());
    }

    /**
     * @test
     */
    public function it_subscribes_user_to_proper_plan_when_plan_group_is_given()
    {
        $this->loadFixtures([
            RoleFixtures::class,
            UserFixtures::class,
            PlanGroupFixtures::class,
            PlanFixtures::class,
        ]);

        $this->subscriptionService->subscribeUserToPlanGroup(static::USER_ID, 1);

        /** @var Subscription $subscription */
        $subscription = $this->subscriptionRepository->findOneBy([
            'user' => static::USER_ID,
            'plan' => static::PLAN_ID
        ]);
        $this->assertEquals(
            (new DateTime())->format('d-m-Y'), // subsription starts today
            $subscription->getStartDate()->format('d-m-Y')
        );
        $interval = $subscription->getStartDate()->diff(
            $subscription->getEndDate()
        );

        $this->assertEquals('uniqueProviderId', $subscription->getPlan()->getProviderId());
    }

    /**
    * @test
    */
    public function it_resets_subscription_when_it_exists_in_database()
    {
        $this->loadFixtures([
            RoleFixtures::class,
            UserFixtures::class,
            PlanGroupFixtures::class,
            PlanFixtures::class,
        ]);

        $this->subscriptionService->subscribeUserToPlanGroup(static::USER_ID, 1);

        /** @var Subscription $subscription */
        $subscription = $this->subscriptionRepository->findOneBy([
            'user' => static::USER_ID,
            'plan' => static::PLAN_ID
        ]);

        $pastStartDate = new DateTime('2011-01-01');
        $pastEndDate = new DateTime('2011-02-01');

        $subscription
            ->setStartDate($pastStartDate)
            ->setActive(false)
            ->setEndDate($pastEndDate);

        $this->subscriptionRepository->persist($subscription);

        $this->subscriptionService->subscribeUserToPlanGroup(static::USER_ID, 1);

        /** @var Subscription $subscription */
        $subscription = $this->subscriptionRepository->findOneBy([
            'user' => static::USER_ID,
            'plan' => static::PLAN_ID
        ]);

        $this->assertTrue($subscription->isActive());

        $this->assertEquals(
            (new DateTime())->format('d-m-Y'), // subsription starts today
            $subscription->getStartDate()->format('d-m-Y')
        );
        $interval = $subscription->getStartDate()->diff(
            $subscription->getEndDate()
        );
        $this->assertEquals(static::PLAN_DURATION, $interval->days);
    }

}
