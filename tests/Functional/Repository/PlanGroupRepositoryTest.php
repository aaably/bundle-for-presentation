<?php

namespace Tests\BillingBundle\Repository;

use Fitatu\BillingBundle\DataFixtures\ORM\PlanFixtures;
use Fitatu\BillingBundle\DataFixtures\ORM\PlanGroupFixtures;
use Fitatu\Local\Repository\Auth\PlanGroupRepository;
use Fitatu\Local\Repository\Auth\PlanRepository;
use Symetria\ApiServerBundle\Tests\AbstractApiWebTestCase;
use Symetria\UserBundle\DataFixtures\ORM\User\RoleFixtures;
use Symetria\UserBundle\DataFixtures\ORM\User\UserFixtures;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PlanGroupRepositoryTest extends AbstractApiWebTestCase
{
    const PLAN_GROUP_ID = '';

    /**
     * @var PlanRepository
     */
    private $planRepository;

    /**
     * @var
     */
    private $planGroupRepository;

    public function setUp()
    {
        parent::setUp();
        $this->loadFixtures([
            RoleFixtures::class,
            UserFixtures::class,
            PlanGroupFixtures::class,
            PlanFixtures::class,
        ]);

        $this->planRepository = $this->getContainer()->get(PlanRepository::class);
        $this->planGroupRepository = $this->getContainer()->get(PlanGroupRepository::class);

    }
    /**
     * @test
     */
    public function it_finds_plans_by_locale_and_provider()
    {
        $this->markTestIncomplete('In progress');
    }
}
