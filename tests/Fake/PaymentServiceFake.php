<?php

namespace Tests\BillingBundle\Fake;

use Fitatu\BillingBundle\Provider\BillingProviderInterface;
use Fitatu\BillingBundle\Service\PaymentService;
use Fitatu\DatabaseBundle\Entity\Auth\Plan;
use Fitatu\SharedUserBundle\Model\User;
use Mockery;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class PaymentServiceFake extends PaymentService
{
    /**
     * @var BillingProviderInterface
     */
    private $billingProvider;

    /**
     * @param BillingProviderInterface $provider
     */
    public function setBillingProvider(BillingProviderInterface $provider)
    {
        $this->billingProvider = $provider;
    }

    /**
     * @return BillingProviderInterface
     */
    protected function getBillingProvider(): BillingProviderInterface
    {
        return $this->billingProvider;
    }

    /**
     * @param User $user
     * @param Plan $plan
     */
    protected function recordPayment($user, $plan): void
    {
        /** @var Plan $plan */

        $plan = Mockery::mock(Plan::class);

        parent::recordPayment(
            new User(),
            $plan
        );
    }
}
