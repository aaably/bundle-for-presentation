<?php

namespace Tests\BillingBundle\Fake;

use Fitatu\SharedUserBundle\Model\User;
use Fitatu\SharedUserBundle\Model\User\UserEntity\UserAppTrait;
use Fitatu\SharedUserBundle\Model\User\UserEntityTrait;
use Symetria\SharedBundle\Model\UserInterface;

/**
 * @author    Sebastian Szczepański
 * @copyright Fitatu Sp. z o.o.
 */
class UserFake extends User implements UserInterface
{
    use UserEntityTrait,
        UserAppTrait;

    const DEFAULTS = [
        'id'                => 1,
        'username'          => 'fakeUser',
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct(
            array_merge(
                static::DEFAULTS,
                $attributes
            )
        );
    }


    /**
     * @param string[] $attributes
     * @return User
     */
    public static function make(array $attributes = []): User
    {
        $attributes = array_merge(
            [
                'id' => static::DEFAULTS['id']
            ],
            $attributes
        );
        return new static($attributes);
    }

}
